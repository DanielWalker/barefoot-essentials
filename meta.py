meta = {
	"name": "Barefoot Essentials",
	"version": "3.0.2",
	"namespace": "Barefoot Monkey",	# Identifies the author - please change this if you make your own variant
	"default_prev_version": '2.27.1'
}
