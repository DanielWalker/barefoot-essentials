var last_BE_version

function inject_request(args) {
	var meta_prefix = "BFM-meta-"

	function unique_id() {
		while (true) {
			var id = "rnd-id-" + Date.now() + "-" + Math.floor(Math.random() * 10000000)
			if (document.getElementById(id) === null) return id
		}
	}
	var id = unique_id()

	var script = document.createElement('script')

	script.appendChild(document.createTextNode(
		'var request = new XMLHttpRequest();'
		+'request.open('+ JSON.stringify(args.method) +', ' + JSON.stringify(args.url) +');'
		+'request.addEventListener("load", function() {'
		+'	var meta = document.head.querySelector("meta[name=' + meta_prefix + id + ']");'
		+'	meta.setAttribute("value", JSON.stringify({status: this.status, statusText: this.statusText, responseText: this.responseText}));'
		+'});'
		+'request.addEventListener("error", function() {'
		+'	var meta = document.head.querySelector("meta[name=' + meta_prefix + id + ']");'
		+'	meta.setAttribute("value", JSON.stringify({status: this.status, statusText: this.statusText, responseText: this.responseText}));'
		+'});'
		+'request.send();'
	))

	var meta = document.createElement('meta')
	meta.setAttribute("name", meta_prefix + id)
	meta.setAttribute("value", meta_prefix + id)
	document.head.appendChild(meta)

	function gen_callback(args, script, meta) {
		return function(mutations) {
			for (var i in mutations) {
				var mutation = mutations[i]
				if (mutation.type == 'attributes' && mutation.attributeName == 'value') {
					observer.disconnect()
					document.head.removeChild(meta)
					document.body.removeChild(script)

					var value = mutation.target.getAttribute('value')
					var response = JSON.parse(value)
					if (response.status == 200) {
						if (args.onload) {
							args.onload.call(args, response)
						}
					} else if (args.onerror) {
						args.onerror.call(args, response)
					}
				}
			}
		}
	}
	
	var callback = gen_callback(args, script, meta)

	var observer = new MutationObserver(callback)
	observer.observe(meta, {attributes: true})
	
	document.body.appendChild(script)
}

function compat_get_gogData() {
	var gogData = unsafeWindow.productcardData
	if (undefined === gogData) {
		var scripts = document.querySelectorAll('body>script:not([src])')
		for (var i = 0; i < scripts.length; i += 1) {
			var script = scripts[i];
			script = script.firstChild
			if (script.nodeType == 3) {
				var text = script.textContent
				if (text.search('var productcardData = ') >= 0) {
					eval(text + ';window.gogData = productcardData')
					break
				}
			}
		}
	}
}

function detect_forum_skin() {
	if (document.head.querySelector('link[href*="forum_carbon"]')) forum_skin = 0
	else forum_skin = 1
}

function add_preview_styles() {
	var style = $('<style>')
	.text(
		'.BE-preview a {'
		+	'background: url("'+((forum_skin==0)?"/www/forum_carbon/-img/post_un.a8689b98.gif":"http://static.gog.com/www/forum_alu/-img/zig_underl.8b625731.gif")+'") repeat-x scroll center bottom transparent;'
		+	'color: ' + ((forum_skin==0)?'#DBDBDB':'#4C4C4C')+';'
		+'}'
		+'.quick_post .BE-preview {'
		+	'margin: 0 0 0 168px;'
		+	'padding: 3px;'
		+	'width: 751px;'
		+	'height: auto;'
		+	'overflow: hidden;'
		+'}'
		+'.BE-preview {'
		+	'height: 150px;'
		+	'overflow: auto;'
		+	'margin: 0 0 0 143px;'
		+	'padding: 5px;'
		+	'clear: both;'
		+	'width: 650px;'
		+	'word-wrap: break-word;'
		+	'color: ' + ((forum_skin==0)?'#DBDBDB':'#4C4C4C')+';'
		+	'transition: height 0.5s ease 0s;'
		+	'font-family: Arial;'
		+	'font-size: 12px;'
		+'}'
		+'.BE-preview .syntax-warning {'
		+	'background: #6F3E3E;'
		+	'border-radius: 5px 5px 5px 5px;'
		+	'color: #DBDBDB;'
		+	'display: inline-block;'
		+	'font-size: 11px;'
		+	'font-weight: normal;'
		+	'font-style: normal;'
		+	'font-family: monospace, sans-serif;'
		+	'margin: 0 5px;'
		+	'padding: 0 2px;'
		+'}'
		+'.BE-preview blockquote {'
		+	'border-left: 1px solid #929292;'
		+	'padding: 0 0 0 8px;'
		+	'margin: 0;'
		+'}'
	)
	.appendTo(document.head)
}

/*-- Quick post/post preview --*/
function submit_quick_post() {
	if (submit_quick_post.submitted) return
	if (location.pathname == "/forum/ajax/popUp") {
		$('.kontent>.submit>div.gog_btn:first-child').click()
	} else {
		var post_text_e = $('.quick_post textarea')
	
		if (post_text_e.length < 1 || post_text_e.val() == '') return
	
		var post_text = post_text_e.val()
		var reply_to = post_text.match(/\[quote_([0-9]+)\]/)
		var reply_to_pid = (reply_to === null) ? undefined : reply_to[1]
					
		$('.submit-quick-post')[0].disabled = true
					
		// submit the post
		submit_quick_post.submitted = true
		$.ajax({
			type:"POST",
			url:"/forum/ajax",
			timeout:15000,
			data:{
				a:"addPost",
				f:$("#f").val(),
				f_arr:$("#f_arr").val(),
				w:$("#w").val(),
				pid:reply_to_pid,
				text:post_text,
				added_images_ids:"",
				added_images_names:"",
				kap:undefined,
				guest_name:undefined,
				btn:"0"
			}
		})
		.done(function(data, textStatus, jqXHR){
			submit_quick_post.submitted = false
			var response = JSON.parse(data)
			if (response.error) {
				alert("A problem occurred while submitting this Quick Post. Try again using a regular post.")
				$('.submit-quick-post')[0].disabled = false
			} else {
				window.location = response.result
			}
		})
		.fail(function(data, textStatus, jqXHR){
			submit_quick_post.submitted = false
			$('.submit-quick-post')[0].disabled = false
			alert("A problem occurred while submitting this Quick Post. Try again using a regular post.")
		})
	}
}
function tag_input_text(input, begin_tag, end_tag) {
	var start = input.selectionStart, end = input.selectionEnd
	input.value = (
		input.value.substring(0, start) 
		+ begin_tag
		+ input.value.substring(start, end)
		+ end_tag
		+ input.value.substring(end)
	)
						
	input.selectionStart = start + begin_tag.length
	input.selectionEnd = end + begin_tag.length
}

function parse_node(node) {
	switch (node.nodeType) {
		case 3: { // text node
			return node.nodeValue
		}
		case 1: { // element
			
			var result = ""
			var after = ""
			
			if (node.tagName == 'BR') return "\n"
			else if (node.tagName == 'DIV') {
				if (node.classList.contains('post_text_c')) {
				} else return ""
			} else if (node.tagName == 'A') {
				result = "[url="+encodeURI(node.getAttribute('href'))+"]"
				after = "[/url]"
			} else if (node.tagName == 'I') {
				result = "[i]"
				after = "[/i]"
			} else if (node.tagName == 'SPAN') {
				if (node.classList.contains('podkreslenie')) {
					result = "[u]"
					after = "[/u]"
				} else if (node.classList.contains('bold')) {
					result = "[b]"
					after = "[/b]"
				} else return ""
			} else return ""
			
			
			var child = node.firstChild
			while (!!child) {
				result += parse_node(child)
				child = child.nextSibling
			}
			
			return result + after
		}
	}
	return ""
}

function parse_post(post) {
	return parse_node(post.find('.post_text_c')[0])
}
function post_keydown_handler(event) {
	if (event.ctrlKey && !event.altKey) {
		switch (event.which) {
			case 66:
			case 98: if (!event.repeat) tag_input_text(this, '[b]', '[/b]'); event.preventDefault(); break
					
			case 85:
			case 117: if (!event.repeat) tag_input_text(this, '[u]', '[/u]'); event.preventDefault(); break
					
			case 73:
			case 105: if (!event.repeat) tag_input_text(this, '[i]', '[/i]'); event.preventDefault(); break
					
			case 76:
			case 108: if (!event.repeat) tag_input_text(this, '\n\n[spoiler]\n\n\n\n', '\n\n\n\n[/spoiler]\n\n'); event.preventDefault(); break
					
			case 81:
			case 113: if (!event.repeat) tag_input_text(this, '[quote]', '[/quote]'); event.preventDefault(); break
					
			case 89:
			case 121: {
				if (!event.repeat) {
					var selected_text = this.value.substring(this.selectionStart, this.selectionEnd)
							
					var url = prompt("Enter the URL for the link", selected_text)
					if (!!url) {
						tag_input_text(this, '[url='+encodeURI(url)+']', '[/url]')
					}
				}
				event.preventDefault()
				break
			}
					
			case 13: {
				if (!event.repeat) submit_quick_post();
				event.preventDefault()
				break
			}
					
			default: return true
		}
		
		event.stopPropagation();
		show_preview()
				
		return false
	}
}
function post_preview_html(source) {
	var tokenexp = /\[\/?(?:[ibu]|(?:url(?:=[^\n\]]*)?|quote(?:_[0-9]*)?))\]/g
	
	var text_tokens = source.split(tokenexp)
	if (!text_tokens) text_tokens = []
	var tag_tokens = source.match(tokenexp)
	if (!tag_tokens) tag_tokens = []
	
	var text_i = 0, tag_i = 0
	var tag_stack = []
	var preview = ""
	var top_tag = null
	var ignore_first_linebreak = false;
	
	while (true) {
		if (text_i < text_tokens.length) {
		
			var text_token = text_tokens[text_i++]
			.replace(/&/g, '&amp;')
			.replace(/\>/g, '&gt;')
			.replace(/\</g, '&lt;')
			.replace(/\n/g, '<br/>')
			
			preview += ignore_first_linebreak ? text_token.replace(/^<br\/>/, '') : text_token
			
		} else break;
		
		if (tag_i < tag_tokens.length) {
		
			var tag = tag_tokens[tag_i++].match(/^\[(\/?)([^=_\]]*)(?:[=_](.*))?\]/)			
			if (tag[2] === undefined) continue
			
			ignore_first_linebreak = false
			
			if (tag[1] == '/') {
			
				if (!!top_tag) {
					preview += top_tag.closing
					
					if (top_tag.tag == 'quote') ignore_first_linebreak = true
					
					if (tag[2] != top_tag.tag)
					preview += '<span class="syntax-warning">[/'+top_tag.tag+']</span>';
					
					top_tag = tag_stack.pop()
				}
				
			} else {
				tag_stack.push(top_tag)
				switch (tag[2]) {
					case 'i': {
						preview += '<i>'
						top_tag = {
							tag: tag[2],
							closing: '</i>'
						}
					} break;
					case 'b': {
						preview += '<b>'
						top_tag = {
							tag: tag[2],
							closing: '</b>'
						}
					} break;
					case 'u': {
						preview += '<u>'
						top_tag = {
							tag: tag[2],
							closing: '</u>'
						}
					} break;
					case 'quote': {
						preview += '<blockquote>';
						ignore_first_linebreak = true
						top_tag = {
							tag: tag[2],
							closing: '</blockquote>'
						}
					} break;
					case 'url': {
						preview +=	(tag[3] === undefined)? '<a href="">' : ('<a href="'+encodeURI(tag[3])+'">');
						top_tag = {
							tag: tag[2],
							closing: '</a>'
						}
					} break;
					default: top_tag = tag_stack.pop();
				}
			}
		}
	}

	while (!!top_tag) {
		preview += top_tag.closing
		preview += '<span class="syntax-warning">[/'+top_tag.tag+']</span>';
		top_tag = tag_stack.pop()
	}

	return preview
}
function show_preview() {
	$('.BE-preview').html(post_preview_html($('.quick_post textarea, form#f_text>textarea#text').val()))
}

function fetch_user_info() {
	return new Promise((resolve, reject) => 
		inject_request({
			method: "GET",
			url: '/userData.json',
			onload: function(response) {
				var user_info = JSON.parse(response.responseText)
				let result = {
					id: user_info.userId,
					galaxy_id: user_info.galaxyUserId,
					avatar: user_info.avatar,
					username: user_info.username,
					updates: user_info.updates,
					timestamp: Date.now()
				}
				resolve(result)
			},
			onerror: function(response) {
				reject("Barefoot Essentials: could not load user data")
			}
		})
	)
}

/*-- global variables - don't judge me --*/
var forum_skin = null
var gog_sync_element = null
var sync_status_account = null
var sync_status_movies = null
var sync_status_games = null
var sync_status_wishlist = null
var sync_status_progress = null
var sync_status_start = null
var sync_status_restart = null
var sync_status_send = null
var sync_status_output = null

var global_user_info
var get_user_info

var sessionStorage_forum_check_key = "BE forum check user:"
var sessionStorage_forum_check_timeout = 1000*60*2

var style_discount_colour_huge = '#4a2cc3'
var style_discount_colour_large = '#2ca3c3'
var style_discount_colour_medium = '#58b600'
var style_discount_colour_small = '#9b8e02'


/*-- utility functions --*/
function cmpVersion(a, b) {
	var i, cmp, len, re = /(\.0)+[^\.]*$/;
	a = (a + '').replace(re, '').split('.');
	b = (b + '').replace(re, '').split('.');
	len = Math.min(a.length, b.length);
	for( i = 0; i < len; i++ ) {
		cmp = parseInt(a[i], 10) - parseInt(b[i], 10);
		if( cmp !== 0 ) {
			return cmp;
		}
	}
	return a.length - b.length;
}
function inject_script(source) {
	var script_text = source
 	var script = unsafeWindow.document.createElement('script')
	script.appendChild(unsafeWindow.document.createTextNode(script_text))
	unsafeWindow.document.body.appendChild(script)
	function remove_element() {
		this.parentElement.removeChild(this)
	}
	setTimeout(remove_element.bind(script), 1)
}
