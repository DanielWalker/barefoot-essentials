from src.Feature import Feature

class FeatureClass(Feature):

	name = "forum_quotation_style"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			( "Forums", {
				"type": 'choice',
				"options": ['normal', 'distinct', 'clear'],
				"def": 'distinct',
				"key": 'forum-quotation-style',
				"label": 'quotation style'
			})
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		switch (value) {
			case 'distinct':
				style.text(
					'.post_text_c div.quot {'
					+	'font-style: normal;'
					+'}'
					+'.post_text_c div.quot:not(.gog_color),'
					+'.post_text_c div.quot:not(.gog_color) a,'
					+'.post_text_c div.quot:not(.gog_color) span.bold,'
					+'.BE-preview blockquote,'
					+'.BE-preview blockquote a {'
					+	'color: ' + ((forum_skin==0)?'#A8A8A8':'#686868')+';'
					+'}'
				)
				break;
			case 'clear':
				style.text(
					'.post_text_c div.quot div.quot {'
					+	'background: transparent;'
					+'}'
					+'.post_text_c div.quot {'
					+	'font-style: normal;'
					+	'padding: 2px 5px 2px 7px;'
					+	'background: ' + ((forum_skin==0)?'rgba(0, 0, 0, 0.1)':'rgba(255, 255, 255, 0.2)')+';'
					+'}'
					+'.post_text_c div.quot:not(.gog_color),'
					+'.post_text_c div.quot:not(.gog_color) a,'
					+'.post_text_c div.quot:not(.gog_color) span.bold,'
					+'.BE-preview blockquote,'
					+'.BE-preview blockquote a {'
					+	'font-size: 12px;'
					+'}'
				)
				break;
			default:
				style.text('')
		}
	}

	var style = $('<style>').appendTo(document.head)

	settings.onchange('forum-quotation-style', on_update)
"""

feature = FeatureClass()