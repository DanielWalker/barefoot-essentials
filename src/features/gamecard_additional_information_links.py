from src.Feature import Feature

class FeatureClass(Feature):

	name = "gamecard_additional_information_links"

	def get_zones(self):
		return ["gamecard", "moviecard"]

	def get_settings(self):
		return [
			("Misc", {
				"type": 'multibool',
				"options": { 'GOG Database': True, 'PCGamingWiki': True, "MaGog": False, 'Wikipedia': True },
				"def": 'GOG.com',
				"key": 'gamecard-additional-information-links',
				"label": 'Additional information links'
				}
			)
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		list.empty()

		// if (value["Forum"]) {
		// 	var url = gogData.cardProduct.forumUrl
		// 	if (url != 'https://www.gog.com/forum/general') {
		// 		list.append(
		// 			$("<li>").append(
		// 				$('<a target="_blank" class="details__link">')
		// 				.attr('href', url)
		// 				.text('Discuss this product on the forum')
		// 			)
		// 		)
		// 	}
		// }
		if (value["GOG Database"]) {
			list.append(
				$("<li>").append(
					$('<a target="_blank" class="details__link">')
					.attr('href', "https://www.gogdb.org/product/"+gogData.cardProduct.id)
					.text('GOG Database')
				)
			)
		}
		// if (value["GOG Wiki"]) {
		// 	list.append(
		// 		$("<li>").append(
		// 			$('<a target="_blank" class="details__link">')
		// 			.attr('href', "https://gogwiki.com/wiki/"+escape(gogData.cardProduct.title))
		// 			.text('GOG Wiki')
		// 		)
		// 	)
		// }
		if (value["PCGamingWiki"]) {
			list.append(
				$("<li>").append(
					$('<a target="_blank" class="details__link">')
					.attr('href', "https://pcgamingwiki.com/wiki/"+escape(gogData.cardProduct.title))
					.text('PCGamingWiki')
				)
			)
		}
		if (value["MaGog"]) {
			list.append(
				$("<li>").append(
					$('<a target="_blank" class="details__link">')
					.attr('href', "http://www.an-ovel.com/cgi-bin/magog.cgi?scp=gdspuriolnc&dsp=ipgfsorlcmbaxyzXhDFGHIJKATSWv0512348&flt=iid~"+gogData.cardProduct.id+"~")
					.text('MaGog')
				)
			)
		}
		if (value["Wikipedia"]) {
			list.append(
				$("<li>").append(
					$('<a target="_blank" class="details__link">')
					.attr('href', "https://en.wikipedia.org/wiki/"+escape(gogData.cardProduct.title))
					.text('Wikipedia')
				)
			)
		}
	}

	var list = $("<ul>")

	$('.details__category.table__row-label').each(function() {
		if (this.textContent.trim() == 'Links:') {
			var sibling = this.nextElementSibling
			list.appendTo(sibling)
		}
	})

	var gogData = unsafeWindow.productcardData

	if (gogData) {
		var style = $('<style>').appendTo(document.head)
		style.text(''
			+'.details__content li {'
			+'	margin-top: 1em;'
			+'}'
		)
		settings.onchange('gamecard-additional-information-links', on_update)
	}
"""

feature = FeatureClass()