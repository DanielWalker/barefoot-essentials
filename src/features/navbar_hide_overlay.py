from src.Feature import Feature

class FeatureClass(Feature):

	name = "navbar_hide_overlay"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			("Navigation bar", { "type": 'bool', "def": True, "key": 'nav-hide-overlay', "label": 'Remove the dark menu overlay' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) style.appendTo(document.head)
		else style.remove()
	}

	var style = $('<style>').text(""
		+"	.menu-overlay {"
		+"	display: none !important;"
		+"}"
	)

	settings.onchange('nav-hide-overlay', on_update)
"""

feature = FeatureClass()