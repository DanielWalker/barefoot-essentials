from src.Feature import Feature

class FeatureClass(Feature):

	name = "click_own_title"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": True, "key": 'forum-title-settings', "label": 'click on own title to change forum settings' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) {
			$('.edit_h_EN').each(function() {
				var post = $(this).closest('.big_post_main')
				if (post) {
				var stat = post.find('.b_u_stat')
				if (stat) {
					var stat_text = stat.text()
					stat.empty().append($('<a href="/forum/mysettings" title="Click here to change your forum title">').text(stat_text))
				}
				}
			})
		} else {
			$('.b_u_stat>a').each(function() {
				var parent = $(this).parent()
				$(this).remove()
				parent.text($(this).text())
			})
		}
	}

	$('<style>').text('.b_u_stat>a { color: inherit; }').appendTo(document.head)

	settings.onchange('forum-title-settings', on_update)
"""

feature = FeatureClass()