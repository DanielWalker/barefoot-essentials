from src.Feature import Feature

class FeatureClass(Feature):

	name = "essentials_link"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return []

	def get_implementation(self):
		return """
	separator = $('.js-menu-about .menu-submenu-separator.menu-submenu-extension-settings')
	if (separator.length == 0) {
		separator = $('<div class="menu-submenu-separator menu-submenu-extension-settings"></div>')
		separator.insertAfter($('.js-menu-about .menu-submenu-separator').last())
	}

	$('<a class="menu-submenu-link">').text('Barefoot Essentials')
	.appendTo(
		$('<div class="menu-submenu-item menu-submenu-item--hover">')
		.insertBefore(separator)
	)
	.click(popup.show.bind(popup, 'Changelog'))
"""

feature = FeatureClass()