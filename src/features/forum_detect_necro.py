from src.Feature import Feature

class FeatureClass(Feature):

	name = "forum_detect_necro"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": True, "key": 'forum-detect-necro', "label": 'Detect "necroposts"', "comment": 'This option scans forum topics for posts made more than 30 days after the previous post, and visibly labels them.' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) {
			$(document.body).addClass("BEfeature-forum-detect-necro")
		} else {
			$(document.body).removeClass("BEfeature-forum-detect-necro")
		}

		feature_promo_show_discount.prev_value = value
	}

	$('<style>').text('body.BEfeature-forum-detect-necro .BE-necro::before {'
	+'content: attr(data-be-necro);'
	+'display: block;'
	+'margin: 1em 4em;'
	+'text-align: center;'
	+'background: #ffcf00;'
	+'color: #000;'
	+'padding: 0.5em 2em;'
	+'line-height: 1;'
	+'border-radius: 1em;'
	+'font-weight: bold;'
	+'z-index: 1;'
	+'}'
	).appendTo(document.head);

	var necro_time = 1000*60*60*24*30

	var now = (new Date()).getTime()
	var relative_date_regex = /^([0-9]+) (hour|minute|day)s? ago$/
	var prev_timestamp = null
	var elements = document.querySelectorAll('.post_date')
	for (var i = 0; i < elements.length; i += 1) {
		var e = elements[i]
		var text = e.textContent.replace('Posted ', '').trim()
		var timestamp = null
		if (text == 'now') timestamp = now
		else if (text == 'Yesterday') timestamp = now - 1000*60*60*24
		else if (text.endsWith(' ago')) {
			var regex_result = relative_date_regex.exec(text)
			if (regex_result.length > 2) {
				var difference = null
				switch (regex_result[2]) {
					case "minute": difference = regex_result[1]*1000*60; break
					case "hour": difference = regex_result[1]*1000*60*60; break
					case "day": difference = regex_result[1]*1000*60*60*24; break
				}
				timestamp = now - difference
			}
		} else timestamp = (new Date(text)).getTime()

		if (timestamp) {
			if (prev_timestamp && timestamp - prev_timestamp > necro_time) {
				$(e).closest('.spot_h').addClass('BE-necro').attr('data-BE-necro', Math.round((timestamp - prev_timestamp) / (1000*60*60*24)) + " days later...")
			}
			prev_timestamp = timestamp
		} else prev_timestamp = null
	}

	setTimeout(settings.onchange.bind(settings, 'forum-detect-necro', on_update), 1)
"""

feature = FeatureClass()