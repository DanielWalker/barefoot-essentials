from src.Feature import Feature

class FeatureClass(Feature):

	name = "forum_avatar_style"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			( "Forums", {
				"type": 'choice',
				"options": ['square', 'rounded corners', 'circle'],
				"def": 'square',
				"key": 'forum-avatar-style',
				"label": 'Avatar style'
			})
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		switch (value) {
			case 'rounded corners':
				settings.set('forum-improve-avatar-shadow', true)
				style.text(
					"div.b_p_avatar_h img, div.small_avatar_2_h img {"
					+"	border-radius: 15%;"
					+"}"
				)
				break
			case 'circle':
				settings.set('forum-improve-avatar-shadow', true)
				style.text(
					"div.b_p_avatar_h img, div.small_avatar_2_h img {"
					+"	border-radius: 50%;"
					+"}"
				)
				break
			default:
				style.text("")
		}
	}

	$('<style>')
	.text(
		"div.b_p_avatar_h img, div.small_avatar_2_h img {"
		+"	box-shadow: 1px 1px 2px 0px #3B3B3B;"
		+"}"
		+"div.b_p_avatar_h, div.small_avatar_2_h {"
		+"	background: none;"
		+"}"
	)
	.appendTo(document.head)

	var style = $('<style>')
	.appendTo(document.head)

	setTimeout(settings.onchange.bind(settings, 'forum-avatar-style', on_update), 1)
"""

feature = FeatureClass()