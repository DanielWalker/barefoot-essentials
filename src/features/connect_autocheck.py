from src.Feature import Feature

class FeatureClass(Feature):

	name = "connect_autocheck"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			("Misc", { "type": 'bool', "def": True, "key": 'connect-autocheck', "label": 'Automatically check for GOG Connect offers' }),
		]

	def get_implementation(self):
		return """
	get_user_info.then((user_info) => {
		var connect_dismissed = null

		function dismiss_alert(items) {
			var dismissed = {
				timestamp: Date.now(),
				items: []
			}
			for (var id in items) {
				dismissed.items.push(id)
			}
			storage.set('connect_dismissed', JSON.stringify(dismissed))

			$('.BE-connect-alert').remove()
		}

		function on_update(value) {
			if (value) style.appendTo(document.head)
			else style.remove()

			if (value && !feature_connect_autocheck.already_checked) {
				feature_connect_autocheck.already_checked = true
			
				if (user_info && user_info.id) inject_request({
					method: "GET",
					url: "/api/v1/users/"+user_info.id+"	/gogLink/steam/exchangeableProducts",
					onload: function(response) {
						var result = JSON.parse(response.responseText)
						if (result) {
							if (result.items && Object.keys(result.items).length > 0) {
								// check to see if current set of games has already been dismissed
								var anything_new = true
								if (connect_dismissed) {
									anything_new = false
									for (var id in result.items) {
										if (-1 == $.inArray(id, connect_dismissed.items)) {
											anything_new = true
											break
										}
									}
								}

								// add alert
								if (anything_new) {
									var connect_notice = $('<div class="BE-connect-alert">')
									var link = $('<a href="https://www.gog.com/connect" target="_blank">').html('New games on <b>GOG Connect</b>').appendTo(connect_notice)
									var dismiss = $('<span>').appendTo(connect_notice)
									connect_notice.appendTo(document.body)

									// add dismiss action
									dismiss.on('click', dismiss_alert.bind(null, result.items))
								}
							}
						} else if (connect_dismissed) {
							storage.delete('connect_dismissed')
							connect_dismissed = null
						}
					},
					onerror: function(response) {
						console.log("could not check Connect")
					}
				})

				$('<style>').text(""
				+"	.BE-connect-alert a {"
				+"	color: #E7B8FF;"
				+"}"
				+"	.BE-connect-alert a:hover {"
				+"	color: #fff;"
				+"}"
				+"	.BE-connect-alert {"
				+"	animation: BE-connect-alert 0.75s;"
				+"	background: #261b5f;"
				+"	position: fixed;"
				+"	z-index: 1000;"
				+"	font-size: 11px;"
				+"	top: 0;"
				+"	left: 75px;"
				+"	padding: 0.2em 1.5em 0.3em;"
				+"	border-radius: 0 0 0.3em 0.3em;"
				+"}"
				+"	.BE-connect-alert span:hover:after {"
				+"	content: 'Click to dismiss';"
				+"}"
				+"	.BE-connect-alert span:hover {"
				+"	color: #fff;"
				+"}"
				+"	.BE-connect-alert span:before {"
				+"	content: '\\u274C ';"
				+"}"
				+"	.BE-connect-alert span {"
				+"	color: #E7B8FF;"
				+"	line-height: 1;"
				+"	cursor: pointer;"
				+"	padding-left: 1em;"
				+"	box-sizing: border-box;"
				+"}"
				+"	.BE-connect-alert:hover {"
				+"	background: #5a2269;"
				+"}"
				+"	@keyframes BE-connect-alert {"
				+"	from {"
				+"	opacity: 0;"
				+"}"
				+"	to {"
				+"	opacity: 1;"
				+"}"
				+"}"
				).appendTo(document.head)
			}
		}

		$('<style>').text(".BE-connect-alert { display: none; }").appendTo(document.head)
		var style = $('<style>').text(".BE-connect-alert { display: block; }")

		storage.get("connect_dismissed", function(connect_dismissed_json) {
			if (connect_dismissed_json) {
				connect_dismissed = JSON.parse(connect_dismissed_json)
				if (!connect_dismissed || !connect_dismissed.timestamp || connect_dismissed.timestamp < Date.now() - 3600000 * 24 * 56) {
					connect_dismissed = null
					storage.delete('connect_dismissed')
				}
			}

			settings.onchange('connect-autocheck', on_update)
		})
	})
"""

feature = FeatureClass()