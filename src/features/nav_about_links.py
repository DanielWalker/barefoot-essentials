from src.Feature import Feature

class FeatureClass(Feature):

	name = "nav_about_links"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			("Navigation bar", { "type": 'multibool', "options": { 'GOG Galaxy': True, 'GOG Downloader': True, 'GOG Connect': True }, "key": 'navbar-about-links', "label": 'About menu links' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {

		galaxy.toggle(value["GOG Galaxy"])
		downloader.toggle(value["GOG Downloader"])
		connect.toggle(value["GOG Connect"])

		separator.toggle(value["GOG Galaxy"] || value["GOG Downloader"] || value["GOG Connect"])
		
	}

	var about_menu = $('.js-menu-about')
	var galaxy = about_menu.find('a.menu-submenu-link[href="/galaxy"]').parent()
	var downloader = $('<div class="menu-submenu-item menu-submenu-item--hover">')
	var connect = $('<div class="menu-submenu-item menu-submenu-item--hover">')
	var separator = galaxy.nextAll('.menu-submenu-separator')

	connect.hide().append($('<a class="menu-submenu-link" href="/connect">').text("GOG Connect")).insertAfter(galaxy)
	downloader.hide().append($('<a class="menu-submenu-link" href="/downloader">').text("GOG Downloader")).insertAfter(galaxy)

	setTimeout(settings.onchange.bind(settings, 'navbar-about-links', on_update), 1)
"""

feature = FeatureClass()