from src.Feature import Feature

class FeatureClass(Feature):

	name = "nav_display_updates"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			("Navigation bar", { "type": 'bool', "def": False, "key": 'nav-display-updates', "label": 'Display notification for new game updates' }),
		]

	def get_implementation(self):
		return """
	get_user_info.then((user_info) => {
		function on_update(value) {
			if (value && user_info.updates.products > 0) {
				$('#menuUsername').attr('data-BE-updates', user_info.updates.products)
			} else $('#menuUsername').removeAttr('data-BE-updates')
		}

		$('<style>')
		.text(
			"#menuUsername[data-BE-updates]:before {"
			+"	content: '\\u2191' attr(data-BE-updates);"
			+"	color: #F4D514;"
			+"	position: absolute;"
			+"	left: 100%;"
			+"	white-space: nowrap;"
			+"	font-size: 14px;"
			+"	padding: 0;"
			+"	margin-left: 25px;"
			+"}"
			+"#menuUsername {"
			+"	position: relative;"
			+"	display: inline-block;"
			+"}"
		)
		.appendTo(document.head)

		setTimeout(settings.onchange.bind(settings, 'nav-display-updates', on_update), 1)
	})
"""

feature = FeatureClass()