from src.Feature import Feature

class FeatureClass(Feature):

	name = "forum_move_edit_note"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": True, "key": 'forum-move-edit-note', "label": 'restyle "post edited" note on edited posts' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) {
			style.text(
				'.big_post_main:hover .BE-edit-note .pencil_text {'
				+	'visibility: visible;'
				+'}'
				+'.BE-edit-note .pencil_text {'
				+	'visibility: hidden;'
				+'}'
				+'.BE-edit-note {'
				+	'padding-top: 4px;'
				+	'left: 180px;'
				+	'position: absolute;'
				+'}'
			)
			$('.post_attaczments .pencil_text').each(
				function() {
					var element = $(this).parent()

					var edit_note = $('<div class="BE-edit-note">')

					element.closest('.big_post_main').find('.post_header_h .p_button_left_h')
					.after(edit_note)

					element.remove()
					element.appendTo(edit_note)
				}
			)
		} else {
			style.text('')
			$('.BE-edit-note .post_attaczments').each(
				function() {
					var element = $(this)
					var parent = element.parent()
					element.remove()

					parent.closest('.big_post_main').find('.post_text')
					.after(element)

					parent.remove()
				}
			)
		}
	}

	var style = $('<style>').appendTo(document.head)

	settings.onchange('forum-move-edit-note', on_update)
"""

feature = FeatureClass()