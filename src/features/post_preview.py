from src.Feature import Feature

class FeatureClass(Feature):

	name = "post_preview"

	def get_zones(self):
		return ["forum/popup"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": True, "key": 'forum-post-preview', "label": 'preview on post/reply window' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) {
			style.text(
				'.text_1, .text_1_bad {'
				+	'height: 180px;'
				+'}'
				+'div.files {'
				+	'z-index: 1;'
				+'}'
				+'div.submit {'
				+	'float: right;'
				+	'clear: none;'
				+	'overflow: visible;'
				+	'padding: 11px 3px 0 0;'
				+'}'
				+'html {'
				+	'height: 100%;'
				+	'overflow: auto;'
				+'}'
				+'div.files {'
				+	'float: left;'
				+	'width: 460px;'
				+	'overflow: hidden;'
				+	'height: auto;'
				+'}'
				+'.zawartosc {'
				+	'height: 100%;'
				+'}'
				+'.zawartosc .kontent {'
				+	'height: initial;'
				+'}'
			)

			var message = document.getElementById('text')
			var preview = $('<p class="BE-preview">').insertAfter($('.kontent>.submit'))
		
			var refresh_preview = function() {
				var text = post_preview_html(message.value)
			
				preview.html(text)
			}

			// capture the offset of the preview and make it absolute
			preview.css({
				'position': 'absolute',
				'bottom': '0px',
				'height': 'auto',
				'top': preview.offset().top + 'px'
			})
		
			$(message).keydown(post_keydown_handler)
			message.addEventListener('input', refresh_preview)
		
			$('.btn_h>.btn').click(refresh_preview)

			refresh_preview()
		} else {
			$('.BE-preview').remove()
			style.text('')
		}
	}

	var style = $('<style>').appendTo(document.head)
	
	settings.onchange('forum-post-preview', on_update)
"""

feature = FeatureClass()