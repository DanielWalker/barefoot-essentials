from src.Feature import Feature

class FeatureClass(Feature):

	name = "promo_show_discount"

	def get_zones(self):
		return ['promo']

	def get_settings(self):
		return [
			("Promotions", { "type": 'bool', "def": True, "key": 'promo-show-discount', "label": "Display discount % on promo pages" }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) {
			style.appendTo(document.head)
		} else {
			style.remove()
		}
	}

	var style = $('<style>').text(
		".product-row-price--old {"
		+"		position: relative;"
		+"}"
		+".product-row-price--old[data-discount]::before {"
		+"	display: inline-block;"
		+"	content: attr(data-discount);"
		+"	color: #f2f2f2;"
		+"	background: #788795;"
		+"	position: absolute;"
		+"	right: 0;"
		+"	bottom: -1.4em;"
		+"	bottom: calc(-5px - 1em);"
		+"	line-height: 1em;"
		+"	padding: 3px 7px 3px;"
		+"	vertical-align: middle;"
		+"	border-radius: 2px;"
		+"}"
		+".product-row-price--old[data-discount][data-discount-size=huge]::before {"
		+"	background: "+ style_discount_colour_huge  +";"
		+"}"
		+".product-row-price--old[data-discount][data-discount-size=large]::before {"
		+"	background: "+ style_discount_colour_large  + ";"
		+"}"
		+".product-row-price--old[data-discount][data-discount-size=medium]::before {"
		+"	background: "+ style_discount_colour_medium  + ";"
		+"}"
		+".product-row-price--old[data-discount][data-discount-size=small]::before {"
		+"	background: "+ style_discount_colour_small  + ";"
		+"}"
	)

	var price_map = new WeakMap()
	function update_price(old_price, new_price) {
		if (old_price && new_price) {
			var discount = Math.round(new_price.textContent / old_price.textContent * 100 - 100)
			if (isNaN(discount)) {
				old_price.removeAttribute('data-discount')
				old_price.removeAttribute('data-discount-size')
			}
			else {
				old_price.setAttribute('data-discount', discount + '%')
				if (-discount >= 80) old_price.setAttribute('data-discount-size', 'huge')
				else if (-discount >= 60) old_price.setAttribute('data-discount-size', 'large')
				else if (-discount >= 30) old_price.setAttribute('data-discount-size', 'medium')
				else old_price.setAttribute('data-discount-size', 'small')
			}
		}
	}

	function mutation_callback(mutations) {
		mutations.forEach(function(mutation) {

			var target = mutation.target
			var old_price = price_map.get(mutation.target)
			update_price(old_price, mutation.target)
		})
	}

	setTimeout(function() {
		$('.product-row__price').each(function() {
			var old_price = this.querySelector('.product-row-price--old')
			var new_price = this.querySelector('.product-row-price--new>._price')
			if (old_price && new_price) {
				price_map.set(new_price, old_price)
				
				var observer = new MutationObserver(mutation_callback.bind(old_price))
				observer.observe(new_price, {childList: true})
				update_price(old_price, new_price)
			}
		})

		settings.onchange('promo-show-discount', on_update)
	}, 500)
"""

feature = FeatureClass()