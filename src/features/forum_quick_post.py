from src.Feature import Feature

class FeatureClass(Feature):

	name = "forum_quick_post"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": True, "key": 'forum-quick-post', "label": 'enable quick post' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) {
			style.text(
				'.quick_post .BE-hints p {'
				+	'margin: 7px 0;'
				+'}'
				+'.quick_post .BE-hints key {'
				+	'border: 1px solid;'
				+	'margin: 0 1px;'
				+	'padding: 1px 4px;'
				+	'border-radius: 4px;'
				+	'font-family: serif;'
				+'}'
				+'.quick_post.BE-focus .BE-hints.BE-focus {'
				+	'opacity: 1;'
				+'}'
				+'.quick_post.BE-focus .BE-hints {'
				+	'opacity: 0;'
				+'}'
				+'.quick_post .BE-hints.BE-focus {'
				+	'opacity: 0;'
				+'}'
				+'.quick_post .BE-hints {'
				+	'color: ' + ((forum_skin==0)?'#aaa': '#878787')+';'
				+	'position: absolute;'
				+	'margin-top: 13px;'
				+'}'
				+'.quick_post textarea:invalid {'
				+	'box-shadow: none;'
				+	'height: 30px;'
				+'}'
				+'.quick_post > textarea:invalid ~ * {'
				+	'opacity: 0;'
				+'}'
				+'.quick_post > * {'
				+	'transition: opacity ease 0.5s;'
				+'}'
				+'.quick_post {'
				+	'background: url("'+((forum_skin==0)?"/www/forum_carbon/-img/post_bg.851b4700.gif": "/www/forum_alu/-img/post_bg.b7d2258c.gif")+'") repeat-x scroll 0 -161px transparent;'
				+	'border-radius: 5px 5px 5px 5px;'
				+	'margin: 0 auto;'
				+	'min-height: 90px;'
				+	'overflow: hidden;'
				+	'padding: 2px 12px;'
				+	'width: 926px;'
				+'}'
				+'.quick_post h1 {'
				+	'color: ' + ((forum_skin==0)?'#aaa': '#878787')+';'
				+	'font-family: "Lucida Grande",Arial,Verdana,sans-serif;'
				+	'font-size: 10px;'
				+	'font-weight: normal;'
				+	'margin: 10px 0 0 58px;'
				+	'clear: both;'
				+'}'
				+'.quick_post textarea:focus, .quick_post textarea:hover {'
				+	'border: 1px solid '+((forum_skin==0)?'#DBDBDB': '#4C4C4C')+';'
				+'}'
				+'.quick_post textarea:focus {'
				+	'height: 150px;'
				+'}'
				+'.quick_post textarea {'
				+	'background: none repeat scroll 0 0 '+((forum_skin==0)?'#676767': '#D1D1D1')+';'
				+	'border: 1px solid '+((forum_skin==0)?'#929292': '#929292')+';'
				+	'color: ' + ((forum_skin==0)?'#DBDBDB':'#4C4C4C')+';'
				+	'height: 150px;'
				+	'transition: height 0.5s ease 0s;'
				+	'margin: 0 0 5px 168px;'
				+	'padding: 2px;'
				+	'width: 751px;'
				+	'font-family: Arial;'
				+	'font-size: 12px;'
				+'}'
				+'button.BE-button::-moz-focus-inner,'
				+'button.submit-quick-post::-moz-focus-inner {'
				+	'border: none;'
				+'}'
				+'button.BE-button:focus,'
				+'button.submit-quick-post:focus {'
				+	'color: #ffffff;'
				+'}'
				+'button.BE-button:hover,'
				+'button.submit-quick-post:hover {'
				+	'background: linear-gradient(#303030, #393939) repeat scroll 0 0 transparent;'
				+	'background: ' + ((forum_skin==0)?"-webkit-linear-gradient(#303030, #393939)": "-webkit-linear-gradient(#959595, #646464)")+' repeat scroll 0 0 transparent;'
				+	'background: ' + ((forum_skin==0)?"linear-gradient(#303030, #393939)": "linear-gradient(#959595, #646464)")+' repeat scroll 0 0 transparent;'
				+'}'
				+'button.BE-button:active,'
				+'button.submit-quick-post:active {'
				+	'background: ' + ((forum_skin==0)?"#4c4c4c": "")+';'
				+'}'
				+'button.BE-button:disabled,'
				+'button.submit-quick-post:disabled {'
				+	'background: linear-gradient(#757575, #828282) repeat scroll 0 0 transparent;'
				+	'box-shadow: none;'
				+	'color: #525252;'
				+'}'
				+'button.BE-button,'
				+'button.submit-quick-post {'
				+	'background: ' + ((forum_skin==0)?"-webkit-linear-gradient(#393939, #434343)": "-webkit-linear-gradient(#646464, #959595)")+' repeat scroll 0 0 transparent;'
				+	'background: ' + ((forum_skin==0)?"linear-gradient(#393939, #434343)": "linear-gradient(#646464, #959595)")+"	 repeat scroll 0 0 transparent;"
				+	'border: medium none;'
				+	'border-radius: 12px 12px 12px 12px;'
				+	'color: ' + ((forum_skin==0)?"#BEBEBE": "#F0F0F0")+';'
				+	'font-family: arial, sans-serif;'
				+	'font-size: 11px;'
				+	'font-weight: normal;'
				+	'line-height: 25px;'
				+	'cursor: pointer;'
				+	'margin: 10px 0;'
				+	'display: block;'
				+	'padding: 0 15px;'
				+	'vertical-align: middle;'
				+	'transition: color 0.3s ease;'
				+'}'
				+'.quick_post div.submit {'
				+	'float: right;'
				+	'clear: none;'
				+	'overflow: visible;'
				+	'padding: 11px 3px 0 0;'
				+'}'
				+'.big_post_h:hover .BE-quick-reply {'
				+	'display: inline-block;'
				+'}'
				+'.BE-quick-reply:hover {'
				+	'color: ' + ((forum_skin==0)?"#E0E0E0": "#606060")+';'
				+'}'
				+'.BE-quick-reply {'
				+	'background: ' + ((forum_skin==0)?"-webkit-linear-gradient(#656566, rgba(101, 101, 102, 0))": "-webkit-linear-gradient(#E5E5E5, rgba(229, 229, 229, 0))")+';'
				+	'background: ' + ((forum_skin==0)?"linear-gradient(#656566, rgba(101, 101, 102, 0))": "linear-gradient(#E5E5E5, rgba(229, 229, 229, 0))")+';'
				+	'border-radius: 3px 3px 3px 3px;'
				+	'color: ' + ((forum_skin==0)?"#B8B8B8": "#606060")+';'
				+	'display: none;'
				+	'height: 23px;'
				+	'margin: 8px 0 0;'
				+	'padding: 4px 3px 0;'
				+	'text-align: center;'
				+	'text-shadow: ' + ((forum_skin==0)?"0px 1px 1px #333333": "none")+';'
				+	'width: 60px;'
				+	'cursor: pointer;'
				+'}'
			)

			$('.p_button_right_h').each(function() {
				$('<div class="BE-quick-reply">')
				.text('quick reply')
				.appendTo(this)
				.click(function() {
				
					window.scrollTo(0, $('.quick_post').position().top - Math.round(window.innerHeight*0.4))
					
					var textarea = $('.quick_post textarea').focus()
					
					var quoted = $(this).closest('.big_post_h')
					var quote_nr = quoted.find('.post_nr').text()
					
					var val = textarea.val()
					textarea.val(
						((val.length==0)?"":(val+"	\\n\\n"))
						+ '[quote_'+quote_nr+']'
						+ parse_post(quoted)
						+'[/quote]\\n'
					)
					
					show_preview()
				})
			})

		} else {
			$('.BE-quick-reply').remove()

			style.text(
				'.quick_post {'
				+	'display: none;'
				+'}'
			)
		}
	}

	var last_post = $('.spot_h:last')
	if (last_post.length > 0) {
		var new_post = $('<textarea required>')
		var preview = $('<p class="BE-preview">')
		var post_button = $('<button class="submit-quick-post">').text('submit quick post')

		$('<div class="quick_post">')
		.append(
			$('<h1>').text('Quick Post'),

			'<div class="BE-hints"><p>Shortcut: <key>Ctrl</key> <key>Space</key></p></div>',
			'<div class="BE-hints BE-focus">'
			+'<p><key>Ctrl</key> <key>I</key>: Italic</p>'
			+'<p><key>Ctrl</key> <key>B</key>: Bold</p>'
			+'<p><key>Ctrl</key> <key>U</key>: Underline</p>'
			+'<p><key>Ctrl</key> <key>Y</key>: Hyperlink</p>'
			+'<p><key>Ctrl</key> <key>L</key>: Spoiler</p>'
			+'<p><key>Ctrl</key> <key>Enter</key>: Submit</p>'
			+'</div>',

			new_post,
			
			$('<div class="submit">')
			.append(
				post_button
			),
			
			$('<h1>').text('Preview'),
			preview
		)
		.insertAfter(last_post)

		// hints respond to focus
		new_post
		.focus( function() { $(this).closest('.quick_post').addClass('BE-focus') } )
		.blur( function() { $(this).closest('.quick_post').removeClass('BE-focus') } )

		// handle shortcut keys in quick post
		new_post.keydown(post_keydown_handler)
		
		// refresh the preview on quic post input
		new_post[0].addEventListener('input', show_preview)
		
		post_button.click(submit_quick_post)
	}

	$(document).keydown(function(event) {
		if (event.ctrlKey && !event.altKey && !event.repeat && event.which == 32) {
			window.scrollTo(0, $('.quick_post').position().top - Math.round(window.innerHeight*0.4))
			$('.quick_post textarea').focus()
			return false
		}
	})

	var style = $('<style>').appendTo(document.head)

	settings.onchange('forum-quick-post', on_update)
"""

feature = FeatureClass()