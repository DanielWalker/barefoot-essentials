from src.Feature import Feature

class FeatureClass(Feature):

	name = "forum_group_giveaways"

	def get_zones(self):
		return ["forum/section"]

	def get_settings(self):
		return [
			( "Forums", {
				"type": 'choice',
				"options": ['leave as they are', 'group and collapse', 'group and expand', 'group below other topics', 'hide'],
				"def": 'group below other topics',
				"key": 'forum-group-giveaways',
				"label": 'giveaway topics (page refresh required)'
			})
		]

	def get_implementation(self):
		return """
	function on_update(value) {

		if (value == 'group below other topics') {
			giveaway_topics.remove().insertAfter($('#t_norm'))
			.find('.list_bar_h')
			.click(function() {
				$(this).siblings('.list_row_h').slideToggle()
			})
			$('.list_bottom_bg').remove().appendTo(giveaway_topics)
		} else {
			giveaway_topics.remove().insertBefore($('#t_norm'))
			.find('.list_bar_h')
			.click(function() {
				$(this).siblings('.list_row_h').slideToggle()
			})
			$('.list_bottom_bg').remove().appendTo($('#t_norm'))
		}

		switch (value) {
			case 'hide':
				style.text(
					'.BE-giveaway-topics, .BE-giveaway-topic {'
					+	'display: none;'
					+'}'
				)
				break
			case 'group and collapse':
				style.text(
					'.BE-giveaway-topics {'
					+	'display: block;'
					+'}'
					+'.BE-giveaway-topic {'
					+	'display: none;'
					+'}'
				)
				giveaway_topics.children('.list_row_h').hide()
				break
			case 'group and expand':
			case 'group below other topics':
				style.text(
					'.BE-giveaway-topics {'
					+	'display: block;'
					+'}'
					+'.BE-giveaway-topic {'
					+	'display: none;'
					+'}'
				)
				giveaway_topics.children('.list_row_h').show()
				break
			default:
				style.text(
					'.BE-giveaway-topics {'
					+	'display: none;'
					+'}'
					+'.BE-giveaway-topic {'
					+	'display: block;'
					+'}'
				)
		}
	}

	var setting = settings.get('forum-group-giveaways')
	
	var giveaway_topics = $('<div class="favourite_h BE-giveaway-topics">')

	var list = $('<div class="list_row_h">')

	var giveaway_count = 0

	$('#t_norm').find('div.topic_s>a').each(function() {
		if (/giveaway[^/]*$/.test(this.getAttribute('href'))) {
			var row = $(this).closest('.list_row_odd')
			row.clone().appendTo(list)
			row.addClass('BE-giveaway-topic')
			giveaway_count += 1
		}
	})

	if (giveaway_count > 0) {

		var bar = $('<div class="list_bar_h">')
		.append(
			$('<div class="lista_icon_3">').attr('style', 'background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADREAAA0RARg5FhkAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuM4zml1AAAAEWSURBVDhPfdC7SoNBEAXg+cHCUrzgK+RB8iKKhCARERTSiHYWCikCpkgRSJPOYCliYyEWgoriC3hB8hLHc342ywozKT427Nk5ycQA2Nxs0xrUodN0NpTxc5O6SbOcqYcZLNGIPmmYCnR+0Te901nyxvOWljU7L9gpL4tftMf7GbWKO33ZlNplQTsoOEgF9eOiRAWdskCtk7TCIK2gUyv80kuxwvO/FXrXFSJ8CFn0xs6vKkR+Ngyy6E291/aJwTNdMUiU5//g6NLgeVgzSJTnguNxBc/HukGiPBd0RxU8rxyWKM8Fh0OD54nDEuW5YH9g8DyuGiTKc8Fu3+C557BEeS5o9QyeOw5LlOeCrQuD54bDEuUq+AOV2hZAAh3hiwAAAABJRU5ErkJggg==") no-repeat scroll 0px 0px transparent'),
			$('<div class="lista_bar_text">').text("Topics which appear to be giveaways ("+giveaway_count+"	)")
		)
		.css('cursor', 'pointer')
	
		giveaway_topics
		.append(bar, list)
	}

	var style = $('<style>').appendTo(document.head)

	settings.onchange('forum-group-giveaways', on_update)
"""

feature = FeatureClass()