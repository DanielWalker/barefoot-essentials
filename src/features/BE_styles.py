from src.Feature import Feature

class FeatureClass(Feature):

	name = "BE_styles"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			("Misc", { "type": 'choice', "options": ['light'], "def": 'light', "key": 'BE-style', "label": 'Barefoot Essentials style' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		switch (value) {
			default:
				style.text(
					'.BE-error {'
					+	'color: #B80000;'
					+	'font-weight: bold;'
					+'}'
					+'.BE-in-progress {'
					+	'color: #808000;'
					+	'font-weight: bold;'
					+'}'
					+'.BE-success {'
					+	'color: #006000;'
					+	'font-weight: bold;'
					+'}'
					+'.BE-sync-progress>p {'
					+	'margin: 1em 0;'
					+'}'
					+'.BE-sync-progress p>span:nth-child(1) {'
					+	'display: inline-block;'
					+	'text-align: right;'
					+	'margin-right: 0.5em;'
					+	'min-width: 25%;'
					+'}'
					+'.BE-popup h2 {'
					+	'font-size: 14px;'
					+	'font-weight: bold;'
					+	'margin: 1.5em 0 0.5em;'
					+	'color: inherit;'
					+	'text-shadow: none;'
					+	'font-family: inherit;'
					+'}'
					+'.BE-popup .BE-changelog + p {'
					+	'margin: 1em;'
					+	'font-style: italic;'
					+	'-moz-user-select: none;'
					+	'user-select: none;'
					+'}'
					+'.BE-older-changes {'
					+	'padding-top: 1px;'
					+'}'
					+'.BE-changelog li:before {'
					+	'content: "\\\\2022";'
					+	'margin-left: -1em;'
					+	'position: absolute;'
					+'}'
					+'.BE-changelog li small {'
					+	'font-weight: bold;'
					+	'font-family: monospace;'
					+	'color: #000;'
					+	'display: inline-block'
					+'}'
					+'.BE-changelog li {'
					+	'margin: 0.3em 0;'
					+	'display: block;'
					+	'padding-left: 1em;'
					+	'line-height: 1.4;'
					+	'position: relative;'
					+'}'
					+'.BE-changelog {'
					+	'list-style: disc inside none;'
					+	'font-size: 12px;'
					+	'display: block;'
					+	'margin-right: 2em;'
					+'}'
					+'.BE-popup a {'
					+	'cursor: pointer;'
					+	'color: blue;'
					+'}'
					+'.BE-popup a:hover {'
					+	'text-decoration: underline'
					+'}'
					+'.BE-popup input[type=checkbox] {'
					+	'width: auto;'
					+	'margin: 2px 0 0 6px;'
					+'}'
					+'.BE-popup .BE-multibool input[type=checkbox] {'
					+	'vertical-align: middle;'
					+	'margin: 0 0.5em 0 0;'
					+	'line-height: 1'
					+'}'
					+'.BE-popup .BE-multibool label {'
					+	'float: none;'
					+	'width: auto;'
					+	'text-align: left;'
					+	'line-height: 1;'
					+	'margin-bottom: 0.2em'
					+'}'
					+'.BE-popup .BE-multibool {'
					+	'overflow: hidden;'
					+	'padding: 0 0 0 0.5em;'
					+	'border-left: 1px solid #676767;'
					+	'margin-top: 5px;'
					+'}'
					+'.BE-popup input[type=range]::-moz-focus-outer {'
					+	'border: none;'
					+	'border-right: 2px solid #808080;'
					+	'border-left: 2px solid #808080;'
					+'}'
					+'.BE-popup input[type=range] {'
					+	'padding: 0 4px;'
					+	'margin-top: 2px;'
					+	'border: none;'
					+'}'
					+'.BE-popup select {'
					+	'border: 1px solid #808080;'
					+	'font-family: "Lucida Grande",Arial,Verdana,sans-serif;'
					+'}'
					+'.BE-popup select, .BE-popup input, .BE-popup .BE-multibool {'
					+	'font-size: 11px;'
					+	'width: 20em;'
					+	'max-width: 70%;'
					+	'box-sizing: border-box;'
					+'}'
					+'.BE-popup p small {'
					+	'display: block;'
					+	'font-style: italic;'
					+	'clear: both;'
					+'}'
					+'.BE-popup p {'
					+	'margin: 0.5em 0;'
					+	'overflow: hidden;'
					+	'padding-bottom: 2px;'
					+'}'
					+'.BE-popup label {'
					+	'float: left;'
					+	'display: block;'
					+	'width: 40%;'
					+	'clear: both;'
					+	'text-align: right;'
					+	'margin-right: 1em;'
					+	'line-height: 1.8em;'
					+	'cursor: default;'
					+	'-moz-user-select: none'
					+'}'
					+'.BE-popup h1 {'
					+	'font-size: 14pt;'
					+	'font-weight: normal;'
					+	'padding: 0.5em 0 .3em 2em;'
					+	'margin: 0 0 0.7em;'
					+	'line-height: normal;'
					+	'border-bottom: 1px solid #676767;'
					+'}'
					+'.BE-popup {'
					+	'background: #E1E1E1;'
					+	'width: 850px;'
					+	'position: fixed;'
					+	'top: 10%;'
					+	'height: 85%;'
					+	'right: calc(50% - 425px);'
					+	'z-index: 600;'
					+	'box-shadow: 1px 1px 10px 0 black;'
					+	'box-sizing: border-box;'
					+	'display: flex;'
					+	'font-size: 11px;'
					+	'font-family: "Lucida Grande",Arial,Verdana,sans-serif;'
					+	'color: #212121;'
					+'}'
					+'.BE-popup>div {'
					+	'flex: 1 1 auto;'
					+	'padding: 1em;'
					+	'overflow: auto;'
					+'}'
					+'.BE-popup>nav>ul>li:hover {'
					+	'color: inherit;'
					+'}'
					+'.BE-popup>nav>ul>li+li {'
					+	'border-top: 1px solid #676767;'
					+'}'
					+'.BE-popup>nav>ul>li.active {'
					+	'background: #E1E1E1;'
					+	'color: #4A4A4A'
					+'}'
					+'.BE-popup>nav>ul>li:not(.active):hover {'
					+	'color: #fff;'
					+	'text-shadow: 1px 1px 0px black;'
					+	'background: #606060;'
					+'}'
					+'.BE-popup>nav>ul>li {'
					+	'padding: 1em 2em;'
					+	'cursor: pointer;'
					+	'line-height: 1;'
					+	'color: #ffffff;'
					+'}'
					+'.BE-popup>nav>ul {'
					+	'margin: 0;'
					+	'padding: 0;'
					+	'display: block;'
					+'}'
					+'.BE-popup>nav {'
					+	'background: #4A4A4A;'
					+	'color: #E1E1E1;'
					+	'font-size: 11px;'
					+	'font-family: "Lucida Grande",Arial,Verdana,sans-serif;'
					+	'min-width: 145px;'
					+	'-moz-user-select: none;'
					+	'-webkit-user-select: none;'
					+	'user-select: none;'
					+'}'
					+'.main-footer {'
					+	'z-index: 0;'
					+'}'
					+'.BE-video-frame {'
					+	'margin: 0 0 0.5em;'
					+	'display: block;'
					+'}'
					+'div.list_bar_h {'
					+	'z-index: 1;'
					+'}'
				)
				break;
		}
	}

	var style = $('<style>').appendTo(document.head)

	settings.onchange('BE-style', on_update)
"""

feature = FeatureClass()