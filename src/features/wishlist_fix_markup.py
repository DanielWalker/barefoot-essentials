from src.Feature import Feature

class FeatureClass(Feature):

	name = "wishlist_fix_markup"

	def get_zones(self):
		return ['wishlist']

	def get_settings(self):
		return [
			("Misc", { "type": 'bool', "def": True, "key": 'wishlist-fix-markup', "label": 'Fix markup code in the wishlist' }),
		]

	def get_implementation(self):
		return """
	var applied = false

	function on_update(value) {
		var regex = /^(.*?)(<a [^>]*>.*?<\\/a>|<br \\/>)(.*)$/
		var a_regex = /^<a [^>]*?href="([^"]*)"[^>]*>(.*?)<\\/a>$/

		function tokenise(parent, string) {
			var substring = string
			while (substring && substring.length > 0) {
				var result = regex.exec(substring)
				if (result) {
					parent.append(document.createTextNode(result[1]))
					if (result[2] == '<br />') parent.append('<br>')
					else {
						var a_result = a_regex.exec(result[2])
						var a = $('<a class="light_un" target="_blank">').attr('href', a_result[1])
						tokenise(a, a_result[2])
						parent.append(a)
					}
					substring = result[3]
				} else {
					parent.append(document.createTextNode(substring))
					substring = null
				}
			}
		}

		if (value && !applied) {
			$('p.description, p.text').each(function() {
				applied = true
				// TODO skip if no matches
				var code = this.textContent
				$(this).empty()

				tokenise($(this), code.replace(/[\\n\\r]/g, ' '))
			})
		}
	}
	settings.onchange('wishlist-fix-markup', on_update)
"""

feature = FeatureClass()
