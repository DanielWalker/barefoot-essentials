# This feature appears to no longer work on the current site
from src.Feature import Feature

class FeatureClass(Feature):

	name = "paging_shortcut_keys"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return []
# 		return [
# 			("Misc", { "type": 'bool', "def": True, "key": 'paging-shortcut-keys', "label": 'Shortcut keys Ctrl + Left/Right arrow to change page' }),
# 		]

	def get_implementation(self):
		return ""
# 		return """
# 	feature_paging_shortcut_keys.callback = 
# 	document.querySelector('.pagin.list__pagination') ? (function(event) {
# 		if (event.ctrlKey && !event.altKey && !event.shiftKey) {
# 			var active = document.activeElement
# 			if (active.tagName == 'TEXTAREA' || (active.tagName == 'INPUT' && active.getAttribute('type') != 'submit')) {
# 				return
# 			}

# 			switch (event.key) {
# 				case 'ArrowLeft': {
# 					var element = document.querySelector('.pagin.list__pagination>.pagin__prev')
# 					if (element) element.click()
# 					break;
# 				}
# 				case 'ArrowRight': {
# 					var element = document.querySelector('.pagin.list__pagination>.pagin__next')
# 					if (element) {
# 						element.click()
# 					}
# 					break;
# 				}
# 			}
# 		}
# 	})
# 	: document.querySelector('.n_b_b_nr_h') ? (function(event) {
# 		if (event.ctrlKey && !event.altKey && !event.shiftKey) {
# 			var active = document.activeElement
# 			if (active.tagName == 'TEXTAREA' || (active.tagName == 'INPUT' && active.getAttribute('type') != 'submit')) {
# 				return
# 			}

# 			switch (event.key) {
# 				case 'ArrowLeft': {
# 					var element = document.querySelector('.n_b_b_nr_active')
# 					if (element) {
# 						var previous = document.querySelector('.n_b_b_nr_active').parentElement.previousElementSibling
# 						if (previous) {
# 							var a = previous.querySelector('a.n_b_b_nr')
# 							if (a) a.click()
# 						}
# 					}
# 					break;
# 				}
# 				case 'ArrowRight': {
# 					var element = document.querySelector('.n_b_b_nr_active')
# 					if (element) {
# 						var next = document.querySelector('.n_b_b_nr_active').parentElement.nextElementSibling
# 						if (next) {
# 							var a = next.querySelector('a.n_b_b_nr')
# 							if (a) a.click()
# 						}
# 					}
# 					break;
# 				}
# 			}
# 		}
# 	})
# 	: null

# 	function on_update(value) {
# 		if (feature_paging_shortcut_keys.callback) {
# 			if (value) {
# 				document.addEventListener('keypress', feature_paging_shortcut_keys.callback)
# 			} else {
# 				document.removeEventListener('keypress', feature_paging_shortcut_keys.callback)
# 			}
# 		}
# 	}

# 	settings.onchange('paging-shortcut-keys', on_update)
# """

feature = FeatureClass()