from src.Feature import Feature

class FeatureClass(Feature):

	name = "nav_community_links"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			("Navigation bar", { "type": 'multibool', "options": { 'Facebook': True, 'Twitter': True, 'Twitch': True }, "key": 'navbar-community-links', "label": 'Community menu links' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {

		facebook.toggle(value["Facebook"])
		twitch.toggle(value["Twitch"])
		twitter.toggle(value["Twitter"])

		separator.toggle(value["Facebook"] || value["Twitter"] || value["Twitch"])
		
	}

	var community_menu = $('.js-menu-community')
	var facebook = community_menu.find('a.menu-submenu-link[href="https://www.facebook.com/gogcom"]').parent()
	var twitter = community_menu.find('a.menu-submenu-link[href="https://twitter.com/GOGcom"]').parent()
	var twitch = community_menu.find('a.menu-submenu-link[href="https://www.twitch.tv/gogcom"]').parent()
	var separator = facebook.prev('.menu-submenu-separator')

	setTimeout(settings.onchange.bind(settings, 'navbar-community-links', on_update), 1)
"""

feature = FeatureClass()