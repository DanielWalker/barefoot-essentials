from src.Feature import Feature

class FeatureClass(Feature):

	name = "gamecard_show_descriptions"

	def get_zones(self):
		return ["gamecard", "moviecard"]

	def get_settings(self):
		return [
			("Misc", { "type": 'bool', "def": True, "key": 'gamecard-show-descriptions', "label": 'Automatically expand game and movie descriptions' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) {
			style.text(
				'.description__text a.description__more {'
				+	'display: none;'
				+'}'
				+'.description__text[ng-hide=showAll] {'
				+	'display: none !important;'
				+'}'
				+'.description__text[ng-show=showAll] {'
				+	'display: block !important;'
				+'}'
			)
		} else {
			style.text('')
		}
	}

	var style = $('<style>').appendTo(document.head)
	settings.onchange('gamecard-show-descriptions', on_update)
"""

feature = FeatureClass()