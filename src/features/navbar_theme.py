from src.Feature import Feature

class FeatureClass(Feature):

	name = "navbar_theme"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			( "Navigation bar", {
				"type": 'choice',
				"options": ['Default theme', 'Default without the stripe', 'GOG Classic 2012', 'GOG Classic 2014', "Bright black"],
				"def": 'Bright black',
				"key": 'navbar-theme',
				"label": 'Navigation bar theme'
			})
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		switch (value) {
			case 'Default without the stripe':
				style.text(
					"body .menu {"
					+"	top: -4px;"
					+"}"
				)
				break
			case 'Bright black':
				style.text(
					"body .menu {"
					+"	top: -4px;"
					+"}"
					+"body .menu__logo:hover {"
					+"	transform: scale(1.075, 1);"
					+"}"
					+"body .menu__logo {"
					+"	transition: transform 0.5s, left 0.6s ease, top 0.5s ease;"
					+"	background: url(https://static.gog.com/www/default/-img/_nav.3d8b0378.png);"
					+"	background-position: 0 -54px;"
					+"	top: 0;"
					+"	left: 0;"
					+"	height: 0;"
					+"	width: 98px;"
					+"	overflow: hidden;"
					+"	content: '';"
					+"	display: block;"
					+"	position: relative;"
					+"	padding-top: 23px;"
					+"	margin-top: 17px;"
					+"}"
				)
				break
			case 'GOG Classic 2012':
				style.text(
					"body .menu {"
					+"	top: -4px;"
					+"}"
					+"body .menu::before {"
					+"	background: linear-gradient(to bottom, #c4c4c4, #c4c4c4 4px, #9b9b9b);"
					+"}"
					+"body .menu__logo {"
					+"	background: url(https://static.gog.com/www/default/-img/_nav.3d8b0378.png);"
					+"	background-position: 0 -54px;"
					+"	height: 0;"
					+"	width: 98px;"
					+"	overflow: hidden;"
					+"	content: '';"
					+"	display: block;"
					+"	position: static;"
					+"	padding-top: 23px;"
					+"	margin-top: 17px;"
					+"}"
					+"body .menu-link {"
					+"	color: #222;"
					+"}"
					+"body .menu-search-input__clear {"
					+"	color: #fff;"
					+"}"
					+"body .menu-search-input__field {"
					+"	color: #222;"
					+"}"
					+"body .menu-item:hover .menu-search-toolbar__close:hover .menu-icon-svg {"
					+"	fill: #fff;"
					+"}"
					+"body .menu-item:hover .menu-search-toolbar__close .menu-icon-svg,"
					+"body .menu-search-icon {"
					+"	fill: #222;"
					+"}"
					+"body .menu-link__dropdown-icon,"
					+"body .menu-icon-svg {"
					+"	fill: #222;"
					+"}"
					+"body .is-expanded .menu-icon-svg, body .menu-icon-svg {"
					+"	fill: #222;"
					+"}"
					+"body .menu-item__count {"
					+"	opacity: 1;"
					+"}"
					+"body #menuUsername[data-BE-updates]:before {"
					+"	color: #0825ff;"
					+"}"
				)
				break
			case 'GOG Classic 2014':
				style.text(
					"body .menu {"
					+"	top: -4px;"
					+"}"
					+"body .menu::before {"
					+"	background: #E1E1E1;"
					+"}"
					+"body .menu-link__dropdown-icon {"
					+"	display: none;"
					+"}"
					+"body .menu-main .menu-link {"
					+"	border-left: #CECECE 1px solid;"
					+"	padding: 0 35px;"
					+"	font-size: 12px;"
					+"}"
					+"body .menu__logo {"
					+"	padding-top: 3px;"
					+"	height: 42px;"
					+"	width: 42px;"
					+"}"
					+"body .menu__logo::before {"
					+"	display: block;"
					+"	content: '';"
					+"	background: linear-gradient(to bottom, #87c101, #c4da13 14px, #f5d815 18px, #eea803 27px);"
					+"	position: absolute;"
					+"	height: 30px;"
					+"	bottom: 4px;"
					+"	left: 3px;"
					+"	right: 3px;"
					+"	z-index: -1;"
					+"}"
					+"body .menu-search-input__clear {"
					+"	color: #fff;"
					+"}"
					+"body .menu-search-input__field {"
					+"	color: #222;"
					+"}"
					+"body .menu-item:hover .menu-search-toolbar__close:hover .menu-icon-svg {"
					+"	fill: #fff;"
					+"}"
					+"body .menu-item:hover .menu-search-toolbar__close .menu-icon-svg,"
					+"body .menu-search-icon {"
					+"	fill: #222;"
					+"}"
					+"body .menu__logo-icon {"
					+"	fill: #000;"
					+"	height: 42px;"
					+"	width: 42px;"
					+"}"
					+"body .menu-item {"
					+"	line-height: 52px;"
					+"}"
					+"body .menu-link, body .is-expanded .menu-link {"
					+"	color: #222;"
					+"}"
					+"body .menu-main .menu-link:hover {"
					+"	color: #222;"
					+"	background: #EBEBEB;"
					+"}"
					+"body .is-expanded .menu-icon-svg, body .menu-icon-svg {"
					+"	fill: #222;"
					+"}"
					+"body .menu-item__count {"
					+"	opacity: 1;"
					+"}"
					+"body #menuUsername[data-BE-updates]:before {"
					+"	color: #1e9fd2;"
					+"	font-size: 12px;"
					+"	float: right;"
					+"	margin-left: 0.5em;"
					+"	position: static;"
					+"}"
				)
				break
			default:
				style.text('')
		}
	}

	var style = $('<style>').appendTo(document.head)

	settings.onchange('navbar-theme', on_update)
"""

feature = FeatureClass()