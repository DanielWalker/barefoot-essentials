from src.Feature import Feature

class FeatureClass(Feature):

	name = "forum_show_hover_elements"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": False, "key": 'forum-show-hover-elements', "label": 'Always show on-hover elements in forum posts', "comment": "Online/Offline status, PM button, and post link number" }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) {
			style.text(
				'div.on_off_stat {'
				+	'visibility: visible;'
				+'}'
				+'div.p_button_left_h {'
				+	'display: block;'
				+'}'
				+'.BE-edit-note .pencil_text {'
				+	'visibility: visible;'
				+'}'
			)
		} else {
			style.text('')
		}
	}
	var style = $('<style>').appendTo(document.head)

	settings.onchange('forum-show-hover-elements', on_update)
"""

feature = FeatureClass()