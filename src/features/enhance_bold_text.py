from src.Feature import Feature

class FeatureClass(Feature):

	name = "enhance_bold_text"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": True, "key": 'forum-bold-text', "label": 'enhanced bold text' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) style.text(
			'.post_text span.bold, .BE-preview b {'
			+	'font-weight: 800;'
			+'}'
		)
		else style.text('')
	}
	var style = $('<style>').appendTo(document.head)
	
	settings.onchange('forum-bold-text', on_update)
"""

feature = FeatureClass()