from src.Feature import Feature

class FeatureClass(Feature):

	name = "hide_menu_icons"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			("Navigation bar", { "type": 'multibool', "options": { 'Alerts': False, 'Friends': False }, "key": 'nav-hide-icons', "label": 'Hide notification icons' }),
		]

	def get_implementation(self):
		return """
	function on_update(values) {
		if (values.Alerts) {
			$('.menu-notifications').hide()
		} else {
			$('.menu-notifications').show()
		}
		if (values.Friends) {
			$('.menu-friends').hide()
		} else {
			$('.menu-friends').show()
		}
	}

	settings.onchange('nav-hide-icons', on_update)
"""

feature = FeatureClass()