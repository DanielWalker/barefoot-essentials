from src.Feature import Feature

class FeatureClass(Feature):

	name = "navbar_position"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			( "Navigation bar", {
				"type": 'choice',
				"options": ['fixed (normal)', 'absolute'],
				"def": 'fixed (normal)',
				"key": 'navbar-position',
				"label": 'Navigation bar position'
			})
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		switch (value) {
			case 'absolute':
				style.text(
					'nav.menu {'
					+	'position: absolute;'
					+'}'
				)
				break;
			default:
				style.text('')
		}
	}
	var style = $('<style>').appendTo(document.head)
	
	settings.onchange('navbar-position', on_update)
"""

feature = FeatureClass()