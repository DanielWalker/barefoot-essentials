from src.Feature import Feature

class FeatureClass(Feature):

	name = "forum_old_gog_avatar"

	def get_zones(self):
		return ["forum", "forum/section"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": False, "key": 'forum-old-gog-avatar', "label": 'Replace the official "GOG.com" user\'s avatar with old green-and-yellow GOG logo' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) {
			$('img[src="https://images.gog.com/67bea49d4dd1f2ce362f3a482f39d9236acd058dac6d2ba91f25eab737126df5_forum_avatar.jpg"]').attr('src', 'https://images.gog.com/9b1aae00838bf648d83b017801d926f025abf226278d1263e20fd8d0df154445_forum_avatar.jpg')
			style.appendTo(document.head)
		} else {
			$('img[src="https://images.gog.com/9b1aae00838bf648d83b017801d926f025abf226278d1263e20fd8d0df154445_forum_avatar.jpg"]').attr('src', 'https://images.gog.com/67bea49d4dd1f2ce362f3a482f39d9236acd058dac6d2ba91f25eab737126df5_forum_avatar.jpg')
			style.remove()
		}
	}

	var style = $("<style>")
	.text(".general_img { background: url(https://images.gog.com/9b1aae00838bf648d83b017801d926f025abf226278d1263e20fd8d0df154445_forum_avatar.jpg) 0 0; background-size: 100%; }")
	
	settings.onchange('forum-old-gog-avatar', on_update)
"""

feature = FeatureClass()