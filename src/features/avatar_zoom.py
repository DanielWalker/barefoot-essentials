from src.Feature import Feature

class FeatureClass(Feature):

	name = "avatar_zoom"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": True, "key": 'forum-avatar-zoom', "label": 'click on avatars to view at original size' }),
		]

	def get_implementation(self):
		return """
	var zoom_on_click

	function on_update(value) {
		zoom_on_click = value
		if (value) {
			style.text(
				'.spot_h:first-child+.BE-fullsize-avatar {'
				+	'margin-top: 13px;'
				+'}'
				+'.BE-fullsize-avatar {'
				+	'box-shadow: 1px 1px 3px 0px #000000;'
				+	'z-index: 101;'
				+	'margin-left: 12px;'
				+	'margin-top: 30px;'
				+	'max-height: 420px;'
				+	'cursor: zoom-out;'
				+	'position: absolute;'
				+'}'
				+'.b_p_avatar_h img {'
				+	'cursor: zoom-in;'
				+'}'
			)
		} else {
			style.text('')
			$('.BE-fullsize-avatar').click()
		}
	}

	$('.b_p_avatar_h img').click(function() {
		if (!zoom_on_click) return

		var img = $(this)
		var src = img.attr('src')

		if (!img.data('BE-zoomed')) {
		
			img.data('BE-zoomed', true)
			
			// clear all fullsize avatars
			$('.BE-fullsize-avatar').data('BE-thumb', null).remove()
			$('.b_p_avatar_h img').data('BE-zoomed', false)
		
			var post = img.closest('.spot_h')
			
			var fullsize = $('<img alt="" class="BE-fullsize-avatar">')
			.attr('src', src.replace(/_forum_avatar\.jpg$/, '.jpg'))
			.insertBefore(post)
			.one('click', function() {
				$(this).data('BE-thumb').data('BE-zoomed', false)
				$(this).data('BE-thumb', null)
				$(this).remove()
			})
			
			fullsize.data('BE-thumb', img)
			
		}
	})

	var style = $('<style>').appendTo(document.head)

	settings.onchange('forum-avatar-zoom', on_update)
"""

feature = FeatureClass()