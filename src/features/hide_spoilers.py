from src.Feature import Feature

class FeatureClass(Feature):

	name = "hide_spoilers"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": True, "key": 'forum-hide-spoilers', "label": 'hide spoilers' }),
		]

	def get_implementation(self):
		return """
	if (!settings.get('forum-hide-spoilers')) return

	var style = $('<style>').appendTo(document.head)
	style.text(
		'.BE-spoiler {'
		+	'height: 0;'
		+	'display: none;'
		+'}'
		+'.BE-spoiler.BE-visible {'
		+	'height: auto;'
		+	'display: block;'
		+	'border: 1px solid '+((forum_skin==0)?"#676767": "#9a9a9a")+';'
		+	'padding: 1em;'
		+	'margin: 1em;'
		+	'background: ' + ((forum_skin==0)?"#585858": "#D7D7D7")+';'
		+	'border-radius: 0.5em;'
		+'}'
		+'.BE-spoiler .BE-spoiler .BE-spoiler.BE-visible {'
		+	'padding: 1em 5px;'
		+	'margin: 1em 1px;'
		+'}'
	)


	function traverse_to_hide_spoilers(parent) {
		function toggle_spoiler() {
			var visible = !$(this).data('visible')
			$(this).data('visible', visible)
			if (visible) {
				$(this).text("hide spoiler")
				.next("div.BE-spoiler").addClass('BE-visible')
			} else {
				$(this).text("show spoiler")
				.next("div.BE-spoiler").removeClass('BE-visible')
			}
		}
		var openings = []
		for (var n = parent.firstChild; n !== null; n = n.nextSibling) {
			if (n.nodeType == 3) {
				
				if (/^\\s*\\[spoiler\\]\\s*$/.test(n.nodeValue)) {
					openings.push(n)
				} else if (/^\\s*\\[\\/spoiler\\]\\s*$/.test(n.nodeValue)) {
					var opening = openings.pop()
					if (opening === null) continue
					
					var spoiler_button = $('<button class="BE-button">')
					.text("show spoiler")
					.click(toggle_spoiler)
					.insertBefore(opening)
					var spoiler_content = $('<div class="BE-spoiler">')
					.insertAfter(spoiler_button)
					
					var next = opening.nextSibling
					for (nn = opening; nn !== null; nn = next) {
						next = nn.nextSibling
						parent.removeChild(nn)
						if (nn !== opening && nn !== n) spoiler_content.append(nn)
						if (nn === n) break
					}
					
					n = spoiler_content.get(0)
				}
			} else if (n.nodeType == 1) {
				traverse_to_hide_spoilers(n)
			}
		}
	}

	$('.post_text_c').each(function() {
		if (0 <= this.textContent.indexOf('[spoiler]')) {
			traverse_to_hide_spoilers(this)
		}
	})
	
	// remove all extra linebreaks around spoilers
	$('.BE-button').each(function() {
		for (var prev = this.previousSibling; prev; prev = this.previousSibling) {
		
			if (prev.nodeType == 1) {
				if (prev.nodeName != 'BR') break
			} else if (prev.nodeType == 3) {
				if (!/^[\\s]*$/.test(prev.nodeValue)) break
			} else break
			this.parentElement.removeChild(prev)
		}
	})
	$('.BE-spoiler').each(function() {
	
		// strip after the spoiler
		for (var next = this.nextSibling; next; next = this.nextSibling) {
		
			if (next.nodeType == 1) {
				if (next.nodeName != 'BR') break
			} else if (next.nodeType == 3) {
				if (!/^[\\s]*$/.test(next.nodeValue)) break
			} else break
			this.parentElement.removeChild(next)
		}
		
		// strip the front
		for (var next = this.firstChild; next; next = this.firstChild) {
			if (next.nodeType == 1) {
				if (next.nodeName != 'BR') break
			} else if (next.nodeType == 3) {
				if (!/^[\\s]*$/.test(next.nodeValue)) break
			} else break
			this.removeChild(next)
		}
		// strip the tail
		for (var next = this.lastChild; next; next = this.lastChild) {
			if (next.nodeType == 1) {
				if (next.nodeName != 'BR') break
			} else if (next.nodeType == 3) {
				if (!/^[\\s]*$/.test(next.nodeValue)) break
			} else break
			this.removeChild(next)
		}
	})
"""

feature = FeatureClass()