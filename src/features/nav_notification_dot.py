from src.Feature import Feature

class FeatureClass(Feature):

	name = "nav_notification_dot"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			("Navigation bar", { "type": 'multibool', "options": { 'Activity feed': False, 'Forum replies': False }, "key": 'nav-hide-notification-dot', "label": 'Hide notification dot for' }),
		]

	def get_implementation(self):
		return """
	function on_update(values) {
		var rules = "";

		if (values['Activity feed']) 
		rules += ".menu-item.js-menu-account > .menu-link--pending-notifications::before {"
		+"	display: none;"
		+"} "

		if (values['Forum replies']) 
		rules += ".menu-item.js-menu-community > .menu-link--pending-notifications::before {"
		+"	display: none;"
		+"} "

		style.text(rules)
	
	}

	var style = $('<style>').appendTo(document.head)
	settings.onchange('nav-hide-notification-dot', on_update)
"""

feature = FeatureClass()