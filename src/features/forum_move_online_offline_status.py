from src.Feature import Feature

class FeatureClass(Feature):

	name = "forum_move_online_offline_status"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": False, "key": 'forum-move-online-offline-status', "label": 'Move online/offline status to its original position' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) {
			style.text(
				'div.on_off_stat {'
				+	'position: relative;'
				+'}'
				+'div.b_u_on, div.b_u_off {'
				+	'position: absolute;'
				+	'top: -24px;'
				+	'left: 64px;'
				+'}'
			)
		} else {
			style.text('')
		}
	}
	var style = $('<style>').appendTo(document.head)

	settings.onchange('forum-move-online-offline-status', on_update)
"""

feature = FeatureClass()