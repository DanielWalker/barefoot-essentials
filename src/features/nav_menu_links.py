from src.Feature import Feature

class FeatureClass(Feature):

	name = "nav_menu_links"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			("Navigation bar", { "type": 'choice', "options": ['GOG.com', 'GOG Galaxy', 'GOG Connect', 'GOG Downloader'], "def": 'GOG.com', "key": 'navbar-about-menu-link', "label": 'Clicking on \"About\" takes you to a page about' }),
			("Navigation bar", { "type": 'choice', "options": ['All forums', 'General forum', 'Forum replies', 'Chat', 'Friends'], "def": 'All forums', "key": 'navbar-community-menu-link', "label": 'Clicking on \"Community\" takes you to' }),
			("Navigation bar", { "type": 'choice', "options": ['Activity feed', 'Profile', 'Games library'], "def": 'Activity feed', "key": 'navbar-account-menu-link', "label": 'Clicking on your account takes you to your' }),
		]

	def get_implementation(self):
		return """
settings.onchange('navbar-account-menu-link', function on_update(value) {
	switch (value) {
		case 'Profile': $('.menu-item.menu-account > a.menu-link').attr('href', '/u'); break
		case 'Games library': $('.menu-item.menu-account > a.menu-link').attr('href', '/account'); break
		default: $('.menu-item.menu-account > a.menu-link').attr('href', '/feed')
	}
})
settings.onchange('navbar-about-menu-link', function on_update(value) {
	switch (value) {
		case 'GOG Galaxy': $('.menu-item.js-menu-about > a.menu-link').attr('href', '/galaxy'); break
		case 'GOG Connect': $('.menu-item.js-menu-about > a.menu-link').attr('href', '/connect'); break
		case 'GOG Downloader': $('.menu-item.js-menu-about > a.menu-link').attr('href', '/downloader'); break
		default: $('.js-menu-about > a.menu-link').attr('href', '/about_gog')
	}
})
settings.onchange('navbar-community-menu-link', function on_update(value) {
	switch (value) {
		case 'General forum': $('.menu-item.js-menu-community > a.menu-link').attr('href', '/forum/general'); break
		case 'Forum replies': $('.menu-item.js-menu-community > a.menu-link').attr('href', '/forum/myrecentposts'); break
		case 'Chat': $('.menu-item.js-menu-community > a.menu-link').attr('href', '/account/chat'); break
		case 'Friends': $('.menu-item.js-menu-community > a.menu-link').attr('href', '/account/friends'); break
		default: $('.menu-item.js-menu-community > a.menu-link').attr('href', '/forum')
	}
})
"""

feature = FeatureClass()