from src.Feature import Feature

class FeatureClass(Feature):

	name = "nav_display_notifications"

	def get_zones(self):
		return [None]

	def get_settings(self):
		return [
			("Navigation bar", { "type": 'bool', "def": False, "key": 'nav-display-notifications', "label": 'Display notification for forum replies and chat' }),
		]

	def get_implementation(self):
		return """
	get_user_info.then((user_info) => {
		function on_update(value) {
			chat.toggle(value)
			replies.toggle(value)
			friend.toggle(value)

			var total_alerts = (user_info.updates.unreadChatMessages || 0) + (user_info.updates.forum || 0) + (user_info.updates.pendingFriendRequests || 0)
			if (value && total_alerts > 0) {
				$('#menuUsername').attr('data-BE-alerts', total_alerts)
			} else $('#menuUsername').removeAttr('data-BE-alerts')
		}

		function scrape_forum_replies() {
			var key = sessionStorage_forum_check_key + user_info.id

			function perform_scrape() {
				inject_request({
					method: "GET",
					url: '/forum',
					onload: function(response) {
						var parser = new DOMParser();
						var root = parser.parseFromString(response.responseText, "text/html");
						var element = root.querySelector('#mainTemplateHolder>.sta_container>.hr>a.replies b')
						
						if (element) {

							user_info.updates.forum = Number.parseInt(element.textContent.trim().replace(/[^0-9]*$/, ''))
							update_forum_replies()
							sessionStorage.setItem(key, JSON.stringify({timestamp: Date.now(), forum: user_info.updates.forum}))

						} else {
							user_info.updates.forum = 0
							update_forum_replies()
							sessionStorage.setItem(key, JSON.stringify({timestamp: Date.now(), forum: 0}))
						}
					},
					onerror: function(response) {
						// console.log("Barefoot Essentials: could not load forum page")
					}
				})
			}

			function update_forum_replies(num_replies) {
				var replies = $('<span class="menu-submenu-item__label">').text(user_info.updates.forum)

				if (user_info.updates.forum) replies.appendTo($('.js-menu-account .menu-submenu-item a[href="/forum/myrecentposts"]'))

				on_update(settings.get('nav-display-notifications'))
			}

			var json = sessionStorage.getItem(key)
			if (json) {
				var last_check = JSON.parse(json)
				if (last_check) {
					if (last_check.timestamp + sessionStorage_forum_check_timeout < Date.now()) {
						perform_scrape()
						return
					} else {
						user_info.updates.forum = last_check.forum
						update_forum_replies()
						return
					}
				}
			}
			perform_scrape()
		}

		var chat = $('<span class="menu-submenu-item__label">').text(user_info.updates.unreadChatMessages)
		if (user_info.updates.unreadChatMessages) chat.appendTo($('.js-menu-account .menu-submenu-item a[href="/account/chat"]'))

		var replies = $('<span class="menu-submenu-item__label">').text(user_info.updates.forum)
		if (user_info.updates.forum) replies.appendTo($('.js-menu-account .menu-submenu-item a[href="/forum/myrecentposts"]'))

		var friend = $('<span class="menu-submenu-item__label">').text(user_info.updates.pendingFriendRequests)
		if (user_info.updates.pendingFriendRequests) friend.appendTo($('.js-menu-account .menu-submenu-item a[href="/account/friends"]'))

		$('<style>')
		.text(
			"#menuUsername[data-BE-alerts]:after {"
			+"	content: attr(data-BE-alerts);"
			+"	display: inline-block;"
			+"	color: #ddd;"
			+"	background-color: #a00;"
			+"	line-height: 1;"
			+"	padding: 0.25em 0.25em 0.15em;"
			+"	border-radius: 0.25em;"
			+"	margin: 0 0 0 0.5em;"
			+"	font-size: 11px;"
			+"	position: relative;"
			+"	bottom: 1px;"
			+"}"
		)
		.appendTo(document.head)

		if (user_info.updates.forum === undefined) scrape_forum_replies()
		setTimeout(settings.onchange.bind(settings, 'nav-display-notifications', on_update), 1)
	})
"""

feature = FeatureClass()