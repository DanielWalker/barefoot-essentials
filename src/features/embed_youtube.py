from src.Feature import Feature

class FeatureClass(Feature):

	name = "embed_youtube"

	def get_zones(self):
		return ["forum/topic"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": True, "key": 'forum-embed-videos', "label": 'Video frames on posts with YouTube video links' }),
		]

	def get_implementation(self):
		return """
	$('<style>')
	.text(
		".BE-video-click-to-play span::before {"
		+"	content: '\\u25ba';"
		+"	font-size: 2em;"
		+"	vertical-align: middle;"
		+"	margin-right: 0.5em;"
		+"}"
		+".BE-video-click-to-play span:active {"
		+"	color: #808080;"
		+"}"
		+".BE-video-click-to-play span:hover {"
		+"	background: #111;"
		+"}"
		+".BE-video-click-to-play span {"
		+"	display: inline-block;"
		+"	background: #222;"
		+"	margin: 1em 1em 1em 0;"
		+"	padding: 0.5em 1em;"
		+"	border-radius: 0.5em;"
		+"	color: #dadada;"
		+"	cursor: pointer;"
		+"	-moz-user-select: none;"
		+"	user-select: none;"
		+"}"
		+".BE-link-highlight {"
		+"	outline: 2px solid yellow;"
		+"	outline-offset: 5px;"
		+"}"
		+".BE-video-close {"
		+"	margin-left: auto;"
		+"	display: block;"
		+"	background: #222;"
		+"	width: 2em;"
		+"	text-align: center;"
		+"	margin-top: 1em;"
		+"	cursor: pointer;"
		+"}"
		+".BE-video-close:hover {"
		+"	background: #111;"
		+"}"
	)
	.appendTo(document.head)

	function click_to_play_youtube() {
		var a = $(this)
		unhighlight_link.call(this)
		var code = a.data('video-code')
		var link = a.data('video-link')
		var time = a.data('video-time')

		var close = $('<div class="BE-video-close">').html('&times;')
		.on('click', hide_video_frame)
		.data('video-code', code)
		.data('video-link', link)
		.data('video-time', time)
		.insertAfter(a.parent())

		$('<iframe class="BE-video-frame" width="751" height="422" frameborder="0" allowfullscreen class="BE-video-frame">')
		.attr('src', 'https://www.youtube.com/embed/' + code + "?start=" + time)
		.insertAfter(close)

		a.remove()
	}

	function hide_video_frame() {
		var close = $(this)
		var code = close.data('video-code')
		var link = close.data('video-link')
		var time = close.data('video-time')
		close.next().remove()
		close.remove()
		embed_youtube_video(code, link, time)
	}

	function highlight_link() {
		var link = $(this).data('video-link')
		if (link && link.nodeType == 1) $(link).addClass('BE-link-highlight')
	}

	function unhighlight_link() {
		var link = $(this).data('video-link')
		if (link && link.nodeType == 1) $(link).removeClass('BE-link-highlight')
	}

	function embed_youtube_video(code, a, time) {
		if ($(a).closest('.quot').length == 0) {
			var comment = $(a).closest('.post_text_c')
			var videos = comment.find('.BE-video-click-to-play')
			if (videos.length == 0) {
				videos = $('<div class="BE-video-click-to-play">').appendTo(comment)
			}

			$('<span>').text("Click to play video")
			.data('video-code', code)
			.data('video-link', a)
			.data('video-time', time)
			.on('click', click_to_play_youtube)
			.on('mouseenter', highlight_link)
			.on('mouseleave', unhighlight_link)
			.appendTo(videos)
			
		}
	}

	function remove_video_embedding() {
		$('.BE-video-frame, .BE-video-click-to-play, .BE-video-close').remove()
	}

	function add_video_embedding() {
		remove_video_embedding()

		// embed www.youtube.com/embed links
		$('.post_text_c a[href^="https://www.youtube.com/embed/"]').each(function() {
			var result = /https?\\:\\/\\/www.youtube.com\\/embed\\/([^#?]*).*(?:[&?]t=([0-9]*))?/.exec(this.href)
			if (result.length > 1) embed_youtube_video(result[1], this, result[2] ? parseInt(result[2]) : 0)
		})

		// embed youtu.be links
		$('.post_text_c a[href^="https://youtu.be"]').each(function() {
			var result = /https?\\:\\/\\/youtu\\.be\\/([^?]*).*[?&]t=(?:([0-9]+)h)?(?:([0-9]+)m)?(?:([0-9]+)s)?/.exec(this.href)
			if (result.length > 1) {
				var time = 0
					if (result[4]) time += parseInt(result[4])
					if (result[3]) time += parseInt(result[3]) * 60
					if (result[2]) time += parseInt(result[2]) * 3600
				embed_youtube_video(result[1], this, time)
			}
		})

		// embed www.youtube.com/watch links
		$('.post_text_c a[href^="http://www.youtube.com/watch"], .post_text_c a[href^="https://www.youtube.com/watch"]').each(function() {
			var result = /https?\\:\\/\\/www.youtube.com\\/watch.*[?&]v=([^&]*)/.exec(this.href)
			if (result.length > 1)  {
				var code = result[1]
				var time = 0
				result = /https?\\:\\/\\/www.youtube.com\\/watch.*[?&]t=(?:([0-9]+)h)?(?:([0-9]+)m)?(?:([0-9]+)s)?/.exec(this.href)
				if (result && result.length > 1)  {
					if (result[3]) time += parseInt(result[3])
					if (result[2]) time += parseInt(result[2]) * 60
					if (result[1]) time += parseInt(result[1]) * 3600
				}
				embed_youtube_video(code, this, time)
			}
		})
	}

	function on_update(value) {
		if (value) add_video_embedding()
		else remove_video_embedding()
	}

	settings.onchange('forum-embed-videos', on_update)
"""

feature = FeatureClass()