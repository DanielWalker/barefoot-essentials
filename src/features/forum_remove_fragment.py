from src.Feature import Feature

class FeatureClass(Feature):

	name = "forum_remove_fragment"

	def get_zones(self):
		return ["forum/section"]

	def get_settings(self):
		return [
			("Forums", { "type": 'bool', "def": True, "key": 'forum-remove-fragment', "label": 'Remove the #fragment at the end of topic list URLs' }),
		]

	def get_implementation(self):
		return """
	function on_update(value) {
		if (value) {
			history.replaceState(null, document.title, location.pathname)
		} else {
			history.replaceState(null, document.title, location.pathname + feature_forum_hide_fragment.fragment)
		}
	}

	feature_forum_remove_fragment.fragment = location.hash
	setTimeout(settings.onchange.bind(settings, 'forum-remove-fragment', on_update), 1)
"""

feature = FeatureClass()