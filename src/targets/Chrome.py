import os
from shutil import copy
import zipfile
from src.Target import Target

class TargetClass(Target):
	def __init__(self):
		super().__init__("Chrome")
		self.additional_files = ["gm.js", "jquery.min.js", "BE-logo-16.png", "BE-logo-32.png", "BE-logo-48.png", "BE-logo-96.png", "BE-logo-128.png"]
		self.dst_path: str = self.prepare_build_path()

	def open(self, meta):
		manifest_file = open(os.path.join(self.dst_path, "manifest.json"), "w")
		manifest_file.write(f"""{{
	"version": "{meta["version"]}",
	"name": "{meta['name']}",
	"description": "Adds many improvements to the GOG.com website.",
	"short_name": "BarefootGOG",
	"manifest_version": 2,
	"icons": {{
		"16": "BE-logo-16.png",
		"32": "BE-logo-32.png",
		"48": "BE-logo-48.png",
		"96": "BE-logo-96.png",
		"128": "BE-logo-128.png"
	}},
	"content_scripts": [
		{{
			"matches": ["https://www.gog.com/*"],
			"js": ["jquery.min.js", "gm.js", "content.js"]
		}}
	],
	"permissions": [
		"https://www.gog.com/",
		"storage",
		"tabs"
	]
}}
""")
		manifest_file.close()
		self.__file = open(os.path.join(self.dst_path, "content.js"), "w")
		self.__file.write(f"""
var branch = '{meta['namespace']}/{self.target_name}'
var version = '{meta["version"]}'
var default_prev_version = '{meta["default_prev_version"]}'	// On first use, all versions after this will be shown in the chanelog
""")

		src_path: str = os.path.join(os.path.dirname(os.path.abspath(__file__)), "Chrome")
		for filename in self.additional_files:
			copy(os.path.join(src_path, filename), self.dst_path+"/")

	def append_script(self, script):
		self.__file.write(script)

	def close(self):
		if self.__file is not None:
			self.__file.close()
			self.__file = None

			extension_file = zipfile.ZipFile(os.path.join(self.dst_path, "BarefootEssentialsGOG.zip"), "w", zipfile.ZIP_DEFLATED)
			for filename in ["manifest.json", "content.js", *self.additional_files]:
				extension_file.write(os.path.join(self.dst_path, filename), filename)
			extension_file.close()

target = TargetClass()
