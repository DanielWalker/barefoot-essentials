import os
from src.Target import Target

class TargetClass(Target):

	__file = None

	def __init__(self):
		super().__init__("GreaseMonkey")

	def open(self, meta):
		path: str = self.prepare_build_path()
		self.__file = open(os.path.join(path, "Barefoot Essentials.user.js"), "w")
		self.__file.write(f"""// ==UserScript==
// @name           {meta['name']}
// @namespace      {meta['namespace']}
// @description    Adds many enhancemnts to the GOG.com website
// @include        https://www.gog.com/*
// @exclude        https://www.gog.com/upload/*
// @require        https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js
// @version        {meta["version"]}
// @grant          GM.getValue
// @grant          GM.setValue
// @grant          GM.xmlHttpRequest
// @grant          GM.deleteValue
// @grant          GM_getValue
// @grant          GM_setValue
// @grant          GM_deleteValue
// ==/UserScript==

var branch = '{meta['namespace']}/{self.target_name}'
var version = '{meta["version"]}'
var default_prev_version = '{meta["default_prev_version"]}'	// On first use, all versions after this will be shown in the chanelog

// Greasemonkey 4 compatibility

var storage = {{
	set: (typeof GM_setValue == 'function')
	? function(key, value) {{
		GM_setValue(key, value)
	}} : function(key, value) {{
		GM.setValue(key, value)
	}},

	get: (typeof GM_getValue == 'function')
	? function(key, callback) {{
		var value = GM_getValue(key)
		callback(value)
	}} : function(key, callback) {{
		var promise = GM.getValue(key)
		promise.then(callback)
	}},

	delete: (typeof GM_deleteValue == 'function')
	? function(key, callback) {{
		GM_deleteValue(key)
	}} : function(key, callback) {{
		GM.deleteValue(key)
	}}
}}
""")

	def append_script(self, script):
		self.__file.write(script)

	def close(self):
		if self.__file is not None:
			self.__file.close()
			self.__file = None

target = TargetClass()
