# Base class for build targets - for example, one implementation of Target might generate GreaseMonkey userscripts, while another one might create a Chrome extension

import os
from shutil import rmtree

class Target:
	target_name = None
	build_path = None

	def __init__(self, target_name):
		self.target_name = target_name
		## TODO sanitise target_name
		self.build_path = os.path.join("./dist", target_name)

	def __del__(self):
		self.close()

	def prepare_build_path(self) -> str:
		# create clean output directory
		if os.path.exists(self.build_path):
			if os.path.isfile(self.build_path):
				os.remove(self.build_path)
			else:
				rmtree(self.build_path)

		os.mkdir(self.build_path)
		return self.build_path

	def open(self, meta):
		pass

	def close(self):
		pass

	def append_script(self, script):
		pass
