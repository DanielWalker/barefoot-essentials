import os
import importlib
from shutil import rmtree
from meta import meta
from .feature_manager import feature_manager

def build():

	def read_file(filepath) -> str:
		file = open(filepath, "r")
		content = file.read()
		file.close()
		return content

	targets = []
	features = []

	# load targets and features
	for filename in os.listdir("./src/targets/"):
		if filename != '__init__.py' and filename[-3:] == '.py':
			module = importlib.import_module("src.targets."+filename[:-3], "targets")
			targets.append(module.target)

	for filename in os.listdir("./src/features/"):
		if filename != '__init__.py' and filename[-3:] == '.py':
			module = importlib.import_module("src.features."+filename[:-3], "features")
			features.append(module.feature)

	# begin each target
	for target in targets:
		target.open(meta)

	# add each feature to the settings manager
	for feature in features:
		feature_manager.add_feature(feature)

	changelog = read_file("src/changelog.json")
	misc = read_file("src/misc.js")
	settings = read_file("src/settings.js")
	popup = read_file("src/popup.js")

	# add features to each target
	for target in targets:
		target.append_script(misc)
		target.append_script(feature_manager.get_settings())
		target.append_script(f"var changelog = {changelog}\n")
		target.append_script(settings)
		target.append_script(popup)

		target.append_script(feature_manager.get_implementations())
		target.append_script(feature_manager.get_calls())

	# finish up each target
	for target in targets:
		target.close()
