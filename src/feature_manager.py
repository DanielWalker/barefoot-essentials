# Manages features added to Barefoot Essentials script, generating scripts for each feature's implementation, settings and function call.
# Unfortuntately, this module is currently very specific to Barefoot Essential, and would need to be modified to be used in any other project.

import json

class FeatureManager:
	class SettingsSection:
		def __init__(self, name):
			self.name = name
			self.settings = []

	class FeatureZone:
		def __init__(self, condition: str, subzones = None):
			self.subzones = []
			self.condition = condition
			self.features = []
			self.subzone_map = {}
			self.num_descendants = 0

			if subzones is not None:
				for subzone in subzones:
					assert subzone[1] is not None, "Zone " + subzone[0] + " must have a condition"
					index = len(self.subzones)
					self.subzone_map[subzone[0]] = index
					self.subzones.append(FeatureManager.FeatureZone(subzone[1], subzone[2]))

		def add_call(self, feature_name, zone):
			if len(zone) == 0:
				self.features.append(feature_name)
				self.num_descendants += 1
			else:
				zone_name = zone[0]
				try:
					zone_index = self.subzone_map[zone_name]
					subzone = self.subzones[zone_index]
					success = subzone.add_call(feature_name, zone[1:])
					self.num_descendants += 1
				except:
					print("Feature " + feature_name + " is asking to be added to unknown zone: " + zone_name)

		def render_calls(self, depth = 0) -> str:
			if self.num_descendants > 0:
				result = ""

				body_depth = depth
				indent = "\t" * depth

				if self.condition is not None:
					result += indent + "if (" + self.condition + ") {\n"
					body_depth += 1

				body_indent = "\t" * body_depth

				for feature in self.features:
					result += body_indent + "feature_" + feature + "();\n"

				for subzone in self.subzones:
					subzone_result = subzone.render_calls(body_depth)
					if subzone_result is not None:
						result += subzone_result

				if self.condition is not None:
					result += indent + "}\n"

				return result

			return None

	def __init__(self):
		self.implementations_script = ""
		self.settings_script = None
		self.calls_script = None
		self.sections = [FeatureManager.SettingsSection("Changelog")]
		self.section_map = {"Changelog": 0}


		self.root_zone = FeatureManager.FeatureZone(None, [
			("forum", "/^\\/forum/.test(window.location.pathname)", [
				("section", "/^\\/forum\\/[^/]*(?:\\/(?:page[0-9]+)?)?$/.test(window.location.pathname) && !location.pathname.startsWith('\\/forum\\/ajax\\/popUp')", None),
				("topic", "/^\\/forum\\/[^/]*\\/[^/]*(?:\\/(?:page[0-9]+|post[0-9]+)?)?$/.test(window.location.pathname) && !location.pathname.startsWith('\\/forum\\/ajax\\/popUp')", None),
				("popup", "location.pathname == '/forum/ajax/popUp'", None),
			]),
			("front page", "window.location.pathname == '/'", None),
			("moviecard", "/^\\/movie\\/[^/]*$/.test(window.location.pathname)", None),
			("gamecard", "/^\\/game\\/[^/]*$/.test(window.location.pathname)", None),
			("library", "(new RegExp(\"^/account(?:/(?:games|movies)(?:/(?:shelf|list))?)?$\")).test(window.location.pathname)", None),
			("catalogue", "(new RegExp('^/(?:games|movies)(?:/.*)?$')).test(window.location.pathname)", None),
			("promo", "/^\\/promo\\/[^/]*$/.test(window.location.pathname)", None),
			("feed", "/^\\/feed$/.test(window.location.pathname)", None),
			("wishlist", "/^\\/wishlist(?:\/.*)$/.test(window.location.pathname)", None),
		])

		pass

	def add_feature(self, feature):
		# add call
		for zone in feature.get_zones():
			self.root_zone.add_call(feature.name, zone.split('/') if (type(zone) is str) else [])

		# add implementation
		self.implementations_script += "function feature_"+feature.name+"() {\n"
		self.implementations_script += feature.get_implementation()
		self.implementations_script += "\n}\n"

		# add settings
		settings = feature.get_settings()
		for setting in settings:
			section_name = setting[0]
			section_index = self.section_map.get(section_name, -1)

			if section_index < 0:
				section_index = len(self.sections)
				self.section_map[section_name] = section_index
				self.sections.append(FeatureManager.SettingsSection(section_name))

			section = self.sections[section_index]
			section.settings.append(setting[1])

	def get_implementations(self):
		return self.implementations_script

	def get_settings(self):
		if self.settings_script is None:
			self.settings_script = "config = {\n"
			for section in self.sections:
				self.settings_script += "\t\""+section.name+"\": [\n"
				for setting in section.settings:
					self.settings_script += "\t\t"+json.dumps(setting)+","
				self.settings_script += "\n\t],\n"
			self.settings_script += "}\n"

		return self.settings_script

	def get_calls(self):
		if self.calls_script is None:
			self.calls_script = """
if (location.hostname == 'www.gog.com') {
	settings.initialise(config, function() {
		get_user_info = fetch_user_info()
		if (/^\\/forum/.test(window.location.pathname)) {
			detect_forum_skin()
			add_preview_styles()
		} else if (/^\\/(?:game|movie)\\/[^/]*$/.test(window.location.pathname)) {
			compat_get_gogData()
		}
"""
			self.calls_script += self.root_zone.render_calls(2)

			self.calls_script += """
		// check version, and show changelog if new
		storage.get('last_BE_version', function(last_BE_version) {
			if (last_BE_version === undefined) last_BE_version = default_prev_version
			else if (settings.get('BE-show-changelog') !== false && cmpVersion(last_BE_version, version) < 0) {
				popup.show('Changelog')
			}
			storage.set('last_BE_version', version)
		})
	})
}
"""

		return self.calls_script

feature_manager = FeatureManager()
