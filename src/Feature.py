# Base class for features

class Feature:

	name = None

	def get_zones(self):
		pass

	def get_settings(self):
		pass

	def get_implementation(self):
		pass
