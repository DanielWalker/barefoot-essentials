var settings = {
	get: function(key) {
		var setting = this.settings[key]
		if (setting) return setting.value
		else return undefined
	},
	set: function(key, value) {
		var setting = this.settings[key]

		if (setting) {
			if (setting.value != value) {
				setting.value = value
				this.save()

				for (var i in setting.onchange) {
					setting.onchange[i](value)
				}
			}
		}
	},
	save: function() {
		var saved_settings = {}
		for (var key in this.settings) {
			saved_settings[key] = this.settings[key].value
		}
		storage.set('settings', JSON.stringify(saved_settings))
	},
	onchange: function(key, callback) {
		var setting = this.settings[key]
		if (setting) {
			if (callback) {
				setting.onchange.push(callback)
				callback(setting.value)
			} else {
				this.save()

				for (var i in setting.onchange) {
					var setting_callback = setting.onchange[i]
					setting_callback(setting.value)
				}
			}
		}
	},

	initialise: function(initial_values, done) {

		storage.get('settings', (function(s) {
			var saved_settings = null

			try {
				if (s !== undefined) saved_settings = JSON.parse(s)
			} catch (exception) {
				console.log(exception)
				storage.delete('settings')
			}

			if (!saved_settings) saved_settings = {}

			for (var section_name in initial_values) {
				for (var i in initial_values[section_name]) {
					var item = initial_values[section_name][i]

					if (item.type == 'multibool') {
						item.def = item.options
					}

					var setting = {
						onchange: [],
						value: (saved_settings[item.key] !== undefined) ? saved_settings[item.key] : item.def
					}

					// for "choice" items, verify that the selected value is a valid option
					if (item.type == 'choice') {
						var valid = false
						for (var j in item.options) {
							var option = item.options[j]
							if (option == setting.value) valid = true
						}
						if (!valid) setting.value = item.def
					}
					
					if (item.type == 'multibool') {
						if (setting.value === undefined) {
							setting.value = item.options
						} else {
							// give default to any undefined options
							var keys = Object.keys(item.options)
							for (j = 0; j < keys.length; j += 1) {
								var key = keys[j]
									if (setting.value[key] === undefined) {
									setting.value[key] = item.options[key]
								}
							}
						}
					}

					this.settings[item.key] = setting
				}
			}

			if (done) done()
		}).bind(this))
	},

	settings: {}
}
